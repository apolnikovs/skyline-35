Job Table
=========

OriginalRetailer             string(30)
RetailerLocation             string(100)
DateOfManufacture            date
PolicyNo                     String(20)
ETDDate                      date
Accessories                  String(200)
UnitCondition                String(50)
Insurer                      String(30)
AuthorisationNo              String(20)
DataConsent                  string(1) Y or N
DateBooked                   date
TimeBooked                   time
DateUnitReceived             date
ClosedDate                   date
Status                       string(30)
CompletionStatus             string(30)
JobSite                      string(1) W or F
EngineerCode                 string(20)
EngineerName                 string(50)
ClaimNumber                  string(20)
AuthorisationNumber          string(20)
ClaimTransmitStatus          int
eInvoiceStatus               int
ChargeableLabourCost         decimal(10,4)
ChargeableLabourVATCost      decimal(10,4)
ChargeablePartsCost          decimal(10,4)
ChargeableDeliveryCost       decimal(10,4)
ChargeableDeliveryVATCost    decimal(10,4)
ChargeableVATCost            decimal(10,4)
ChargeableSubTotal           decimal(10,4)
ChargeableTotalCost          decimal(10,4)
AgentChargeableInvoiceNo     integer
AgentChargeableInvoiceDate   date


Parts Table
===========

SBPartID        int
PartStatus      string(30)
OldPartSerialNo string(20)
IsOtherCost     string(1) Y or N
IsAdjustment    string(1) Y or N

Appointments Table
==================

SBAppointID     int

StatusUpdate Table
==================

OldStatus      String(30)
UserCode       String(3)

