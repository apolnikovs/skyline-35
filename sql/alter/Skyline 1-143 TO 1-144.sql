# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.143');


# ---------------------------------------------------------------------- #
# Modify table "network"                                                 #
# ---------------------------------------------------------------------- #

ALTER TABLE `network` ADD COLUMN `ServiceManagerForename` VARCHAR(50) NULL AFTER `ModifiedDate`;
ALTER TABLE `network` ADD COLUMN `ServiceManagerSurname` VARCHAR(50) NULL AFTER `ServiceManagerForename`;
ALTER TABLE `network` ADD COLUMN `ServiceManagerEmail` VARCHAR(100) NULL AFTER `ServiceManagerSurname`;
ALTER TABLE `network` ADD COLUMN `AdminSupervisorForename` VARCHAR(50) NULL AFTER `ServiceManagerEmail`;
ALTER TABLE `network` ADD COLUMN `AdminSupervisorSurname` VARCHAR(50) NULL AFTER `AdminSupervisorForename`;
ALTER TABLE `network` ADD COLUMN `AdminSupervisorEmail` VARCHAR(100) NULL AFTER `AdminSupervisorSurname`;
ALTER TABLE `network` ADD COLUMN `SendASCReport` ENUM('Yes','No') NULL DEFAULT NULL AFTER `AdminSupervisorEmail`;


# ---------------------------------------------------------------------- #
# Modify table "service_provider"                                        #
# ---------------------------------------------------------------------- #

ALTER TABLE `service_provider` ADD COLUMN `ServiceManagerForename` VARCHAR(50) NULL DEFAULT NULL AFTER `AutoSpecifyEngineerByPostcode`;
ALTER TABLE `service_provider` ADD COLUMN `ServiceManagerSurname` VARCHAR(50) NULL DEFAULT NULL AFTER `ServiceManagerForename`;
ALTER TABLE `service_provider` ADD COLUMN `ServiceManagerEmail` VARCHAR(100) NULL DEFAULT NULL AFTER `ServiceManagerSurname`;
ALTER TABLE `service_provider` ADD COLUMN `AdminSupervisorForename` VARCHAR(50) NULL DEFAULT NULL AFTER `ServiceManagerEmail`;
ALTER TABLE `service_provider` ADD COLUMN `AdminSupervisorSurname` VARCHAR(50) NULL DEFAULT NULL AFTER `AdminSupervisorForename`;
ALTER TABLE `service_provider` ADD COLUMN `AdminSupervisorEmail` VARCHAR(100) NULL DEFAULT NULL AFTER `AdminSupervisorSurname`;
ALTER TABLE `service_provider` ADD COLUMN `SendASCReport` ENUM('Yes','No') NULL DEFAULT 'No' AFTER `AdminSupervisorEmail`;

# ---------------------------------------------------------------------- #
# Modify table "status"                                                 #
# ---------------------------------------------------------------------- #

ALTER TABLE `status` ADD COLUMN `TimelineStatus` ENUM('booked','viewed','in_progress','parts_ordered','parts_received','site_visit','revisit','closed') NULL DEFAULT NULL AFTER `StatusName`; 


# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #

insert into version (VersionNo) values ('1.144');