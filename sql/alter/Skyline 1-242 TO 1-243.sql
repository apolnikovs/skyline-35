
# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 
call UpgradeSchemaVersion('1.242');


# ---------------------------------------------------------------------- #
# Add table recon                                                        #
# ---------------------------------------------------------------------- #
CREATE TABLE IF NOT EXISTS `recon` (
  `ReconID` int(11) NOT NULL AUTO_INCREMENT,
  `CourierID` int(11) NOT NULL,
  `FileNumber` char(4) DEFAULT NULL,
  `DateCreated` date DEFAULT NULL,
  `DateUpdated` date DEFAULT NULL,
  `SLNumber` int(11) DEFAULT NULL,
  `ConsignmentNo` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ReconID`),
  KEY `IDX_CourierID` (`CourierID`)
);


# ---------------------------------------------------------------------- #
# Modify table customer                                                  #
# ---------------------------------------------------------------------- #
ALTER TABLE customer 
	ADD SendServiceCompletionText ENUM( 'No', 'MobileNo', 'AlternativeMobileNo' ) NULL DEFAULT 'MobileNo', 
	ADD ContactAltMobile VARCHAR( 40 ) NULL;

# ---------------------------------------------------------------------- #
# Add table sms                                                          #
# ---------------------------------------------------------------------- #
CREATE TABLE IF NOT EXISTS sms ( 
	SMSID int(11) NOT NULL AUTO_INCREMENT, 
	SMSName varchar(64) DEFAULT NULL, 
	SMSBodyText text, 
	CreatedDate timestamp NOT NULL DEFAULT '0000-00-00 00:00:00', 
	EndDate timestamp NOT NULL DEFAULT '0000-00-00 00:00:00', 
	Status enum('Active','In-active') NOT NULL DEFAULT 'Active', 
	ModifiedUserID int(11) DEFAULT NULL, 
	ModifiedDate timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, 
	PRIMARY KEY (SMSID), 
	KEY IDX_mobile_phone_network_ModifiedUserID_FK (ModifiedUserID) 
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


# ---------------------------------------------------------------------- #
# Add table recon                                                        #
# ---------------------------------------------------------------------- #
ALTER TABLE brand 
	ADD SendRepairCompleteTextMessage ENUM( 'No', 'Yes' ) NULL DEFAULT 'No', 
	ADD SMSID INT( 11 ) NULL;


# ---------------------------------------------------------------------- #
# Modify table part_status                                               #
# ---------------------------------------------------------------------- #
CREATE TABLE part_status (
	PartStatusID INT(10) NOT NULL AUTO_INCREMENT, 
	PartStatusName VARCHAR(50) NULL DEFAULT NULL, 
	PartStatusDescription VARCHAR(250) NULL DEFAULT NULL, 
	ServiceProviderID INT(11) NULL DEFAULT NULL, 
	PartStatusDisplayOrder INT(4) NULL DEFAULT NULL, 
	DisplayOrderSection ENUM('Yes','No') NOT NULL DEFAULT 'No', 
	Status ENUM('Active','In-active') NOT NULL DEFAULT 'Active', 
	PRIMARY KEY (PartStatusID) 
) COLLATE='latin1_swedish_ci' ENGINE=InnoDB;


# ---------------------------------------------------------------------- #
# Modify table part                                                      #
# ---------------------------------------------------------------------- #
ALTER TABLE part 
	ADD COLUMN PartStatusID INT(11) NULL DEFAULT NULL AFTER OrderStatus,
	ADD COLUMN SupplierID INT(11) NULL DEFAULT NULL AFTER Supplier;


# ---------------------------------------------------------------------- #
# Add table diary_matrix                                                 #
# ---------------------------------------------------------------------- #
 CREATE TABLE `diary_matrix` (
	`DiaryMatrixID` INT(10) NOT NULL AUTO_INCREMENT,
	`Name` VARCHAR(100) NULL DEFAULT NULL,
	`Date` Date NULL DEFAULT NULL,
	`ServiceProviderID` INT(11) NOT NULL,
	`Key` VARCHAR(50),	
	PRIMARY KEY (`DiaryMatrixID`),
	KEY IDX_diary_matrix_Date (`Date`),
	CONSTRAINT `service_provider_TO_diary_matrix` FOREIGN KEY (`ServiceProviderID`) REFERENCES `service_provider` (`ServiceProviderID`)
) COLLATE='latin1_swedish_ci' ENGINE=InnoDB;

# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.243');
