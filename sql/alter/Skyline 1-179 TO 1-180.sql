# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.179');

# ---------------------------------------------------------------------- #
# Modify table "allocation_reassignment"                                 #
# ---------------------------------------------------------------------- #
CREATE TABLE `allocation_reassignment` (
`AllocationReassignmentID` INT(10) NOT NULL AUTO_INCREMENT,
`MainServiceProviderID` INT(10) NOT NULL,
`SecondaryServiceProviderID` INT(10) NOT NULL DEFAULT '0',
`Postcode` VARCHAR(10) NULL DEFAULT NULL,
PRIMARY KEY (`AllocationReassignmentID`)
);

 
# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #

insert into version (VersionNo) values ('1.180');
