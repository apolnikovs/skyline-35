# ---------------------------------------------------------------------- #
# Script generated with: DeZign for Databases V7.2.0                     #
# Target DBMS:           MySQL 5                                         #
# Project file:          SkyLine.dez                                     #
# Project name:          SkyLine                                         #
# Author:                Brian Etherington                               #
# Script type:           Alter database script                           #
# Created on:            2012-11-08 14:40                                #
# ---------------------------------------------------------------------- #


# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                                     #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.116');

# ---------------------------------------------------------------------- #
# Add table "status_preference"                                          #
# ---------------------------------------------------------------------- #

CREATE TABLE `status_preference` (
    `UserID` INTEGER NOT NULL,
    `StatusID` INTEGER NOT NULL,
    `Page` VARCHAR(40) NOT NULL,
    `Type` VARCHAR(2) NOT NULL
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

CREATE UNIQUE INDEX `IDX_status_preference_1` ON `status_preference` (`UserID`,`StatusID`,`Page`,`Type`);

# ---------------------------------------------------------------------- #
# Add foreign key constraints                                            #
# ---------------------------------------------------------------------- #

ALTER TABLE `status_preference` ADD CONSTRAINT `user_TO_status_preference` 
    FOREIGN KEY (`UserID`) REFERENCES `user` (`UserID`);

# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #

insert into version (VersionNo) values ('1.117');
