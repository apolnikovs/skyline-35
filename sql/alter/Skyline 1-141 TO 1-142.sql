# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.141');


# ---------------------------------------------------------------------- #
# Modify table "job"                                                     #
# ---------------------------------------------------------------------- #

ALTER TABLE `job` ADD `ColAddEmail` VARCHAR(40) NULL AFTER `ColAddPostcode`;
ALTER TABLE `job` ADD `ColAddPhone` VARCHAR(40) NULL AFTER `ColAddEmail`;
ALTER TABLE `job` ADD `ColAddPhoneExt` VARCHAR(10) NULL AFTER `ColAddPhone`; 

# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #

insert into version (VersionNo) values ('1.142');