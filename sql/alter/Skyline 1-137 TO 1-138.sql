# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.137');


# ---------------------------------------------------------------------- #
# Modify table "manufacturer"                                            #
# ---------------------------------------------------------------------- #

#ALTER TABLE `manufacturer` ADD COLUMN `AuthReqChangedDate` TIMESTAMP NULL DEFAULT NULL AFTER AuthorisationRequired;


# ---------------------------------------------------------------------- #
# Modify table "job"                                                     #
# ---------------------------------------------------------------------- #

#ALTER TABLE `job` ADD COLUMN `RepairAuthStatus` ENUM('required','not_required','confirmed','rejected') NULL DEFAULT NULL AFTER Insurer;


# ---------------------------------------------------------------------- #
# Modify table "branch"                                                  #
# ---------------------------------------------------------------------- #

#ALTER TABLE `branch` ADD COLUMN `ServiceManager` VARCHAR(250) NULL DEFAULT NULL AFTER ContactPhoneExt;


# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #

insert into version (VersionNo) values ('1.138');






