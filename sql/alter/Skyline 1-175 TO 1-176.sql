# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.175');

# ---------------------------------------------------------------------- #
# Modify table "branch"                                                  #
# ---------------------------------------------------------------------- #
ALTER TABLE `branch` ADD COLUMN `DefaultServiceProvider` INT(11) NULL DEFAULT NULL AFTER `ServiceAppraisalRequired`;
ALTER TABLE `branch` ADD CONSTRAINT `FK_branch_service_provider` FOREIGN KEY (`DefaultServiceProvider`) REFERENCES `service_provider` (`ServiceProviderID`);

 
# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #

insert into version (VersionNo) values ('1.176');
