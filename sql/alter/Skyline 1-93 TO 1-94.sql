# ---------------------------------------------------------------------- #
# Script generated with: DeZign for Databases V7.2.0                     #
# Target DBMS:           MySQL 5                                         #
# Project file:          SkyLine.dez                                     #
# Project name:          SkyLine                                         #
# Author:                Brian Etherington                               #
# Script type:           Alter database script                           #
# Created on:            2012-10-12 14:45                                #
# ---------------------------------------------------------------------- #


# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                                     #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.93');

# ---------------------------------------------------------------------- #
# Modify table "service_provider_engineer_skillset"                      #
# ---------------------------------------------------------------------- #

ALTER TABLE `service_provider_engineer_skillset` MODIFY `ServiceProviderEngineerSkillsetID` INTEGER NOT NULL AUTO_INCREMENT;

# ---------------------------------------------------------------------- #
# Add table "postcode_lat_long"                                          #
# ---------------------------------------------------------------------- #

CREATE TABLE `postcode_lat_long` (
    `PostCode` VARCHAR(10) NOT NULL,
    `Latitude` FLOAT,
    `Longitude` FLOAT,
    CONSTRAINT `postcode_lat_longID` PRIMARY KEY (`PostCode`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #

insert into version (VersionNo) values ('1.94');
