# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.140');


# ---------------------------------------------------------------------- #
# Modify table "service_type"                                            #
# ---------------------------------------------------------------------- #

CREATE INDEX `IDX_Warranty` ON service_type(Warranty);



# ---------------------------------------------------------------------- #
# Modify table "appointment"                                             #
# ---------------------------------------------------------------------- #

ALTER TABLE `appointment` ADD COLUMN `CompletionEngineerUserCode` CHAR(3) NULL DEFAULT NULL AFTER `CompletedBy`;
ALTER TABLE `appointment` ADD COLUMN `ArrivalDateTime` DATETIME NULL DEFAULT NULL AFTER `CompletionEngineerUserCode`;
ALTER TABLE `appointment` ADD COLUMN `PartsAttached` TINYINT(1) NOT NULL DEFAULT '0' AFTER `ArrivalDateTime`;
ALTER TABLE `appointment` ADD COLUMN `CompletionDateTime` DATETIME NULL DEFAULT NULL AFTER `PartsAttached`;



# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #

insert into version (VersionNo) values ('1.141');






