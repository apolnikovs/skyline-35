# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 
call UpgradeSchemaVersion('1.259');

# ---------------------------------------------------------------------- #
# Modify Table job                                                       #
# ---------------------------------------------------------------------- # 
ALTER TABLE `job`
	ADD COLUMN `InboundFaultCodeID` INT NULL DEFAULT NULL AFTER `RMASendFail`;

# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.260');
