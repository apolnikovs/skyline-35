# ---------------------------------------------------------------------- #
# Script generated with: DeZign for Databases V7.2.0                     #
# Target DBMS:           MySQL 5                                         #
# Project file:          SkyLine.dez                                     #
# Project name:          SkyLine                                         #
# Author:                Brian Etherington                               #
# Script type:           Alter database script                           #
# Created on:            2012-10-16 13:39                                #
# ---------------------------------------------------------------------- #


# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                                     #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.96');

# ---------------------------------------------------------------------- #
# Add table "diary_sp_map"                                               #
# ---------------------------------------------------------------------- #

CREATE TABLE `diary_sp_map` (
    `diarySPMapID` INTEGER NOT NULL AUTO_INCREMENT,
    `ServiceProviderID` INTEGER,
    `MapDate` DATE,
    `MapUUID` VARCHAR(64),
    `dateCreated` TIMESTAMP,
    CONSTRAINT `diary_sp_mapID` PRIMARY KEY (`diarySPMapID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #

insert into version (VersionNo) values ('1.97');
