# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.133');


# ---------------------------------------------------------------------- #
# Create table "table_state"                                             #
# ---------------------------------------------------------------------- #
/*
CREATE TABLE `table_state` (
	`UserID` INT(10) NOT NULL,
	`TableID` VARCHAR(250) NOT NULL,
	`StateData` TEXT NOT NULL,
	PRIMARY KEY (`UserID`),
	INDEX `TableID` (`TableID`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB;
*/

# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #

insert into version (VersionNo) values ('1.134');