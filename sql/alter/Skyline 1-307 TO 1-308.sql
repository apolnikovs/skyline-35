# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 
call UpgradeSchemaVersion('1.307');

# ---------------------------------------------------------------------- #
# Vykintas Rutkunas Changes 											 #
# ---------------------------------------------------------------------- # 
UPDATE open_jobs_status SET OpenJobsStatusCode = 'closed', OpenJobsStatusName = 'Returned to Customer' WHERE open_jobs_status.OpenJobsStatusCode = 'Returned to Customer' AND open_jobs_status.OpenJobsStatusName = 'closed' ;


# ---------------------------------------------------------------------- #
# Praveen Kumar Nakka Changes 											 #
# ---------------------------------------------------------------------- # 
ALTER TABLE job_fault_code_lookup ADD ApproveStatus ENUM( 'Approved', 'Pending' ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'Pending' AFTER Status;
ALTER TABLE job_fault_code ADD ApproveStatus ENUM( 'Approved', 'Pending' ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'Pending' AFTER Status;


# ---------------------------------------------------------------------- #
# Krishnam Raju Nalla Changes 											 #
# ---------------------------------------------------------------------- # 
ALTER TABLE `remote_engineer_history`
	ADD COLUMN `UserCode` VARCHAR(30) NULL DEFAULT NULL AFTER `Longitude`;


# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.308');



