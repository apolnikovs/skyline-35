# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.162');



# ---------------------------------------------------------------------- #
# Modify table "bought_out_guarantee"                                    #
# ---------------------------------------------------------------------- #

ALTER TABLE `bought_out_guarantee` CHANGE COLUMN `ManufacturerID` `ManufacturerID` INT(11) NULL DEFAULT NULL AFTER ClientID;



# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #

insert into version (VersionNo) values ('1.163');
