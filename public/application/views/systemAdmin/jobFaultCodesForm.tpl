 <script type="text/javascript">
 $(document).ready(function() {
 //multi select checkboxes   
 $('#JobFaultCodeLookupID').multiselect( {  noneSelectedText: "{$page['Text']['select_default_option']|escape:'html'}", minWidth: 300, height: "auto", show:['slide', 500]  } );

    // Commented by thirumal
   {* $("#RepairTypeID").combobox();
    {if $datarow.JobFaultCodeID !=''}
        $("#LinkedItem").combobox();
    {/if} *}
    {if $SupderAdmin eq true}
        $("#ServiceProviderID").combobox();
    {/if}
    $(function() {
        $( "#tabs" ).tabs({
            show: function(event, ui) {
                $.colorbox.resize();
            }
        });
    });
if($('#RestrictLength').attr('checked')=="checked"){ $('#addFieldsDop').show();$.colorbox.resize();}else{ $('#addFieldsDop').hide() ;$.colorbox.resize();}
if($('#ForceFormat').attr('checked')=="checked"){ $('#addFieldsDop2').show();$.colorbox.resize();}else{ $('#addFieldsDop2').hide();$.colorbox.resize(); }
if($('#ReqOnlyForRepairType').attr('checked')=="checked"){ $('#addFieldsDop3').show();$.colorbox.resize();}else{ $('#addFieldsDop3').hide();$.colorbox.resize(); }
$('.helpTextIconQtip').each(function()
{
    $HelpTextCode =  $(this).attr("id");
    // We make use of the .each() loop to gain access to each element via the "this" keyword...
    $(this).qtip(
    {
        hide: 'click',
        events: {
            hide: function(){
                $(this).qtip('api').set('content.text', '<img src="{$_subdomain}/images/ajax-loader.gif" '); // Direct API method 
            }
        },
        content: {
            // Set the text to an image HTML string with the correct src URL to the loading image you want to use
            text: '<img src="{$_subdomain}/images/ajax-loader.gif" >',
            ajax: {
                url: '{$_subdomain}/Popup/helpText/' + urlencode($HelpTextCode) + '/Qtip=1/' + Math.random(),
                once: false // Re-fetch the content each time I'm shown
            },
            title: {
                text: 'Help', // Give the tooltip a title using each elements text
                button: true
            }
        },
        position: {
            at: 'bottom center', // Position the tooltip above the link
            my: 'top center',
            viewport: $(window), // Keep the tooltip on-screen at all times
            effect: false // Disable positioning animation
        },
        show: {
            event: 'click',
            solo: true // Only show one tooltip at a time
        },
        style: {
            classes: 'qtip-tipped  qtip-shadow'
        }
    })
}).click(function(event) {
    event.preventDefault()
});
});
</script>
{if $deleteFlag eq 'Yes'}
<div id="deleteDiv">
    <form id="CForm" name="CForm" method="post"  action="" class="inline">
        <input type="hidden" name="JobFaultCodeID" value="{$datarow.JobFaultCodeID}">
        <fieldset>
        <legend>{$page['Text']['delete_page_legend']|escape:'html'}</legend>
        <p id="deleteP">Are you sure you want to delete this '<strong>{$datarow.JobFaultCode}</strong>' Job Fault Code?</p>
            <div style="height:20px; margin-bottom: 10px;text-align: center;">
                <input type="submit" name="delete_save_btn" class="textSubmitButton centerBtn" id="delete_save_btn" value="{$page['Buttons']['yes_btn']|escape:'html'}" />
                <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;" style="float:right; margin-left: 20px;" value="{$page['Buttons']['no_btn']|escape:'html'}" />
            </div>
        </fieldset>
    </form>
</div>
{else}
<div id="JobFaultCodeFormPanel" class="SystemAdminFormPanel" >
<form id="CForm" name="CForm" method="post"  action="" class="inline">
<input type="hidden" name="JobFaultCodeID" value="{$datarow.JobFaultCodeID}">
<fieldset>
<legend title="" >{$page['Labels']['page_lable']|escape:'html'}</legend>
<div id="tabs">
<ul>
<li><a href="#tabs-1">{$page['Labels']['tab1_heading']|escape:'html'}</a></li>
<li><a href="#tabs-2">{$page['Labels']['tab2_heading']|escape:'html'}</a></li>
<li><a href="#tabs-3">{$page['Labels']['tab3_heading']|escape:'html'}</a></li>
<li><a href="#tabs-4">{$page['Labels']['tab4_heading']|escape:'html'}</a></li>
<li><a {if $datarow.JobFaultCodeID !=''}onclick="loadLinkedItem()" {/if} href="#tabs-5">{$page['Labels']['tab5_heading']|escape:'html'}</a></li>
</ul>
<p>
    <label ></label>
    <span class="topText" >{$page['Text']['top_info_text1']|escape:'html'} <span>*</span> {$page['Text']['top_info_text2']|escape:'html'}</span>
</p>
<div id="tabs-1" class="SystemAdminFormPanel inline">
    <p>
        <label class="cardLabel" for="JobFaultCode" >{$page['Text']['job_fault_code_text']|escape:'html'}<sup>*</sup></label>
        &nbsp;
        <input type="text" class="text"  name="JobFaultCode" value="{$datarow.JobFaultCode|default:''}" id="JobFaultCode" >
    </p>
    <p>
        <label class="cardLabel" for="Status" >{$page['Text']['status_text']|escape:'html'}</label>
        &nbsp;
        <span class="formRadioCheckText"  class="saFormSpan">
        <input  type="checkbox" name="Status"  value="In-active" {if $datarow.Status|default:'' eq 'In-active'}checked="checked"{/if}  /><label >Inactive</label>&nbsp;
        </span>
    </p>
    
    {if $SupderAdmin eq true}	
        <p>
            <label class="cardLabel" for="ServiceProviderID">{$page['Text']['serviceProvider_text']|escape:'html'}<sup>*</sup></label>
            &nbsp;
            <select id="ServiceProviderID" name="ServiceProviderID">
            <option value="" {if $datarow.ServiceProviderID eq ""}selected="selected"{/if}>Select Service Provider</option>
                {foreach $splist as $s}
                    <option value="{$s.ServiceProviderID}" {if $datarow.ServiceProviderID eq $s.ServiceProviderID}selected="selected"{/if}>{$s.Acronym}</option>
                {/foreach}
            </select>
        </p>
	{if $datarow.JobFaultCodeID != '' && $datarow.JobFaultCodeID != '0'}
	{* updated by Praveen Kumar N [START] *}
	<p>
        <label class="cardLabel" for="ApproveStatus" >{$page['Text']['approve_status_text']|escape:'html'}</label>
        &nbsp;
	<span class="formRadioCheckText"  class="saFormSpan">
	<input  type="checkbox" name="ApproveStatus"  value="Pending" {if $datarow.ApproveStatus eq 'Pending'}checked="checked"{/if}  /><label>Unapproved</label>
	</span>       
	</p>
	{/if}
	{* [END] *}
	{*
	
	<p>
        <label class="cardLabel" for="ApproveStatus" >{$page['Text']['approve_status_text']|escape:'html'}</label>
        &nbsp;
        <span class="formRadioCheckText"  class="saFormSpan">
        <input  type="radio" name="ApproveStatus"  id="ApproveStatus" value="Approved" {if $datarow.ApproveStatus|default:'' eq 'Approved'}checked="checked"{/if}  /><label >Yes</label>&nbsp; <input  type="radio" name="ApproveStatus"  id="ApproveStatus" value="Pending" {if $datarow.ApproveStatus|default:'' eq 'Pending'} checked="checked"{/if}  /><label >No</label>
        </span>
    </p>
	*}
	
    {else}
        <input type="hidden" name="ServiceProviderID" value="{$sId}" />
	<input type="hidden" name="ApproveStatus" value="Pending" />
    {/if}
    <p>
        <label class="cardLabel" for="FieldNo">{$page['Text']['field_no_text']|escape:'html'}<sup>*</sup></label>
        &nbsp;
        <input style="width:142px"  type="text" class="text"  name="FieldNo" value="{$datarow.FieldNo|default:''}" id="FieldNo">
        <img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="jobFaultCode_FieldNo_Help" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" >
    </p>
    <p>
        <label class="cardLabel" for="FieldType">{$page['Text']['field_type_text']|escape:'html'}</label>
        &nbsp;<span class="formRadioCheckText"><input {if $datarow.FieldType eq $page['Labels']['field_type_label1']}checked=checked{/if}  type="radio"  name="FieldType" value="{$page['Labels']['field_type_label1']}" id="FieldType" >&nbsp;<label class="saFormSpan">{$page['Labels']['field_type_label1']}</label></span>
        <span style="padding-left: 214px;" class="formRadioCheckText"><input {if $datarow.FieldType eq $page['Labels']['field_type_label2']}checked=checked{/if}  type="radio"  name="FieldType" value="{$page['Labels']['field_type_label2']}" id="FieldType" >&nbsp;<label class="saFormSpan">{$page['Labels']['field_type_label2']}</label><br></span>
        <span style="padding-left: 214px;" class="formRadioCheckText"><input {if $datarow.FieldType eq $page['Labels']['field_type_label3']}checked=checked{/if}  type="radio"  name="FieldType" value="{$page['Labels']['field_type_label3']}" id="FieldType" >&nbsp;<label class="saFormSpan">{$page['Labels']['field_type_label3']}</label></span>
        <img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="jobFaultCode_FieldType_Help" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" >
    </p>
    <p>
        <label class="cardLabel" for="Lookup" >{$page['Text']['lookup_text']|escape:'html'}</label>
        &nbsp;
        <span class="formRadioCheckText" class="saFormSpan">
        <input  type="checkbox" name="Lookup"  value="Yes" {if $datarow.Lookup|default:'Yes' eq 'Yes'}checked="checked"{/if}  />
        <img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="jobFaultCode_Lookup_Help" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" >
        </span>
    </p>
    <p>
        <label class="cardLabel" for="MainInFault" >{$page['Text']['maininfault_text']|escape:'html'}</label>
        &nbsp;
        <span class="formRadioCheckText"  class="saFormSpan">
        <input  type="checkbox" name="MainInFault"  value="Yes" {if $datarow.MainInFault eq 'Yes'}checked="checked"{/if}  />
        <img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="jobFaultCode_maininfault_Help" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" >
        </span>
    </p>
    <p>
        <label class="cardLabel" for="MainOutFault" >{$page['Text']['mainoutfault_text']|escape:'html'}</label>
        &nbsp;
        <span class="formRadioCheckText"  class="saFormSpan">
        <input  type="checkbox" name="MainOutFault"  value="Yes" {if $datarow.MainOutFault eq 'Yes'}checked="checked"{/if}  />
        <img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="jobFaultCode_mainoutfault_Help" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" >
        </span>
    </p>
    <p>
        <label class="cardLabel" for="NonFaultCode" >{$page['Text']['nonfaultcode_text']|escape:'html'}</label>
        &nbsp;
        <span class="formRadioCheckText"  class="saFormSpan">
        <input  type="checkbox" name="NonFaultCode"  value="Yes" {if $datarow.NonFaultCode eq 'Yes'}checked="checked"{/if}  />
        <img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="jobFaultCode_nofaultcode_Help" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" >
        </span>
    </p>
    <p>
        <label class="cardLabel" for="UseAsGenericFaultDescription" >{$page['Text']['useasgenericdescription_text']|escape:'html'}</label>
        &nbsp;
        <span class="formRadioCheckText"  class="saFormSpan">
        <input  type="checkbox" name="UseAsGenericFaultDescription"  value="Yes" {if $datarow.UseAsGenericFaultDescription eq 'Yes'}checked="checked"{/if}  />
        <img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="jobFaultCode_UseAsGenericFaultDescription_Help" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" >
        </span>
    </p>
</div>

<div id="tabs-2" class="SystemAdminFormPanel inline">
    <p>
        <label class="cardLabel" for="RestrictLength" >{$page['Text']['restrictlenght_text']|escape:'html'}</label>
        &nbsp;
        <span class="formRadioCheckText"  class="saFormSpan">
        <input onclick="if(this.checked==true){ $('#addFieldsDop').show();$.colorbox.resize();}else{ $('#addFieldsDop').hide();$.colorbox.resize(); }"  type="checkbox" name="RestrictLength" id="RestrictLength"  value="Yes" {if $datarow.RestrictLength eq 'Yes'}checked="checked"{/if}  />
        <img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="jobFaultCode_RestrictLength_Help" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" >
        </span>
    </p>
    <div id="addFieldsDop" style="display:none">
        <p>
            <label class="cardLabel" for="MinLength" >{$page['Text']['minlenght_text']|escape:'html'}<sup>*</sup></label>
            &nbsp;
            <input style="width:142px"  type="text" class="text"  name="MinLength" value="{$datarow.MinLength}" id="MinLength" >
        </p>
        <p>
            <label class="cardLabel" for="MaxLength" >{$page['Text']['maxlenght_text']|escape:'html'}<sup>*</sup></label>
            &nbsp;
            <input style="width:142px"  type="text" class="text"  name="MaxLength" value="{$datarow.MaxLength}" id="MaxLength" >
        </p>
    </div>
    <p>
        <label class="cardLabel" for="ForceFormat" >{$page['Text']['forceformat_text']|escape:'html'}</label>
        &nbsp;
        <span class="formRadioCheckText"  class="saFormSpan">
        <input type="checkbox" name="ForceFormat" id="ForceFormat" value="Yes" {if $datarow.ForceFormat eq 'Yes'}checked="checked"{/if}  />
        <img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="jobFaultCode_ForceFormat_Help" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" >
        </span>
    </p>
    <p>
        <label class="cardLabel" for="RequiredFormat" >{$page['Text']['requiredformat_text']|escape:'html'}</label>
        &nbsp;
        <span class="formRadioCheckText"  class="saFormSpan">
        <input  style="width:142px"  type="text" class="text"  name="RequiredFormat" value="{$datarow.RequiredFormat}" id="RequiredFormat" >
        <img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="jobFaultCode_RequiredFormat_Help" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" >
        </span>
    </p>
</div>

<div id="tabs-3" class="SystemAdminFormPanel inline">
    <p>
        <label class="cardLabel" for="ReplicateToFaultDescription" >{$page['Text']['autoreplicatefaultcodetofaultdescription_text']|escape:'html'}</label>
        &nbsp;
        <span class="formRadioCheckText"  class="saFormSpan">
        <input  type="checkbox" name="ReplicateToFaultDescription" id="ReplicateToFaultDescription"  value="Yes" {if $datarow.ReplicateToFaultDescription eq 'Yes'}checked="checked"{/if}  />
        <img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="jobFaultCode_ReplicateToFaultDescription_Help" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" >
        </span>
    </p>
    <p>
        <label class="cardLabel" for="FillFromDOP" >{$page['Text']['fillfromdop_text']|escape:'html'}</label>
        &nbsp;
        <span class="formRadioCheckText"  class="saFormSpan">
        <input   type="checkbox" name="FillFromDOP" id="FillFromDOP"  value="Yes" {if $datarow.FillFromDOP eq 'Yes'}checked="checked"{/if}  />
        <img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="jobFaultCode_FillFromDOP_Help" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" >
        </span>
    </p> 
    <p>
        <label class="cardLabel" for="HideWhenRelatedFaultCodeBlank" >{$page['Text']['hidewhenrelatedcodeisblank_text']|escape:'html'}</label>
        &nbsp;
        <span class="formRadioCheckText"  class="saFormSpan">
        <input type="checkbox" name="HideWhenRelatedCodeIsBlank" id="HideWhenRelatedCodeIsBlank"  value="Yes" {if $datarow.HideWhenRelatedCodeIsBlank eq 'Yes'}checked="checked"{/if}  />
        <img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="jobFaultCode_HideWhenRelatedCodeIsBlank_Help" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" >
        </span>
    </p>
    <p>
        <label class="cardLabel" for="FaultCodeNo" >{$page['Text']['faultcodeno_text']|escape:'html'}</label>
        &nbsp;
        <input  style="width:142px"  type="text" class="text"  name="FaultCodeNo" value="{$datarow.FaultCodeNo}" id="FaultCodeNo" >
    </p>
</div>

<div id="tabs-4" class="SystemAdminFormPanel inline">
    <p>
        <label class="cardLabel" for="HideForThirdPartyJobs" >{$page['Text']['hideforthirdpartyjobs_text']|escape:'html'}</label>
        &nbsp;
        <span class="formRadioCheckText"  class="saFormSpan">
        <input type="checkbox" name="HideForThirdPartyJobs" id="HideForThirdPartyJobs"  value="Yes" {if $datarow.HideForThirdPartyJobs eq 'Yes'}checked="checked"{/if}  />
        <img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="jobFaultCode_HideForThirdPartyJobs_Help" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" >
        </span>
    </p>
    <p>
        <label class="cardLabel" for="NotReqForThirdPartyJobs" >{$page['Text']['notrequiredforthirdpartyjobs_text']|escape:'html'}</label>
        &nbsp;
        <span class="formRadioCheckText"  class="saFormSpan">
        <input type="checkbox" name="NotReqForThirdPartyJobs" id="NotReqForThirdPartyJobs"  value="Yes" {if $datarow.NotReqForThirdPartyJobs eq 'Yes'}checked="checked"{/if}  />
        <img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="jobFaultCode_NotReqForThirdPartyJobs_Help" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" >
        </span>
    </p>
    <p>
        <label class="cardLabel" for="ReqAtBookingForChargeableJobs" >{$page['Text']['requiredatbookingforchargeable_text']|escape:'html'}</label>
        &nbsp;
        <span class="formRadioCheckText"  class="saFormSpan">
        <input   type="checkbox" name="ReqAtBookingForChargeableJobs" id="ReqAtBookingForChargeableJobs"  value="Yes" {if $datarow.ReqAtBookingForChargeableJobs eq 'Yes'}checked="checked"{/if}  />
        <img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="jobFaultCode_ReqAtBookingForChargeableJobs_Help" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" >
        </span>
    </p>
    <p>
        <label class="cardLabel" for="ReqAtCompletionForChargeableJobs" >{$page['Text']['requiredatcompletionforchargeable_text']|escape:'html'}</label>
        &nbsp;
        <span class="formRadioCheckText"  class="saFormSpan">
        <input type="checkbox" name="ReqAtCompletionForChargeableJobs" id="ReqAtCompletionForChargeableJobs"  value="Yes" {if $datarow.ReqAtCompletionForChargeableJobs eq 'Yes'}checked="checked"{/if}  />
        <img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="jobFaultCode_ReqAtCompletionForChargeableJobs_Help" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" >
        </span>
    </p>
    <p>
        <label class="cardLabel" for="ReqAtBookingForWarrantyJobs" >{$page['Text']['requiredatbookingforwarranty_text']|escape:'html'}</label>
        &nbsp;
        <span class="formRadioCheckText"  class="saFormSpan">
        <input   type="checkbox" name="ReqAtBookingForWarrantyJobs" id="ReqAtBookingForWarrantyJobs"  value="Yes" {if $datarow.ReqAtBookingForWarrantyJobs eq 'Yes'}checked="checked"{/if}  />
        <img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="jobFaultCode_ReqAtBookingForWarrantyJobs_Help" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" >
        </span>
    </p>
    <p>
        <label class="cardLabel" for="ReqAtCompletionForWarrantyJobs" >{$page['Text']['requiredatcompletionforwarranty_text']|escape:'html'}</label>
        &nbsp;
        <span class="formRadioCheckText"  class="saFormSpan">
        <input   type="checkbox" name="ReqAtCompletionForWarrantyJobs" id="ReqAtCompletionForWarrantyJobs"  value="Yes" {if $datarow.ReqAtCompletionForWarrantyJobs eq 'Yes'}checked="checked"{/if}  />
        <img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="jobFaultCode_ReqAtCompletionForWarrantyJobs_Help" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" >
        </span>
    </p>
    <p>
        <label class="cardLabel" for="ReqIfExchangeIssued" >{$page['Text']['requiredifexchangeissued_text']|escape:'html'}</label>
        &nbsp;
        <span class="formRadioCheckText"  class="saFormSpan">
        <input   type="checkbox" name="ReqIfExchangeIssued" id="ReqIfExchangeIssued"  value="Yes" {if $datarow.ReqIfExchangeIssued eq 'Yes'}checked="checked"{/if}  />
        <img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="jobFaultCode_ReqIfExchangeIssued_Help" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" >
        </span>
    </p>
    <p>
        <label class="cardLabel" for="ReqOnlyForRepairType" >{$page['Text']['requiredonlyforrepairtype_text']|escape:'html'}</label>
        &nbsp;
        <span class="formRadioCheckText"  class="saFormSpan">
        <input onclick="if(this.checked==true){ $('#addFieldsDop3').show();$.colorbox.resize();}else{ $('#addFieldsDop3').hide();$.colorbox.resize(); }" type="checkbox" name="ReqOnlyForRepairType" id="ReqOnlyForRepairType"  value="Yes" {if $datarow.ReqOnlyForRepairType eq 'Yes'}checked="checked"{/if}  />
        <img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="jobFaultCode_ReqOnlyForRepairType_Help" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" >
        </span>
    </p>
    <div id="addFieldsDop3" style="display:none">
        <p>
            <label class="cardLabel" for="RepairTypeID" >{$page['Text']['requiredrepairtype_text']|escape:'html'}<sup>*</sup></label>
            &nbsp;
            <select name="RepairTypeID" id="RepairTypeID" class="text" >
                <option value="" {if $datarow.RepairTypeID eq ''}selected="selected"{/if}>Select from drop down</option>
                {foreach $RepairTypes as $s}
                    <option value="{$s.RepairTypeID}" {if $datarow.RepairTypeID eq $s.RepairTypeID}selected="selected"{/if}>{$s.RepairType}</option>
                {/foreach}
            </select>
        </p>
    </div>
</div>                           
<div id="tabs-5" class="SystemAdminFormPanel inline">
    <p>
        <label class="cardLabel" for="UseLookup" >{$page['Text']['uselookup_text']|escape:'html'}</label>
        &nbsp;
        <span class="formRadioCheckText"  class="saFormSpan">
        <input   type="checkbox" name="UseLookup" id="UseLookup"  value="Yes" {if $datarow.UseLookup|default:'Yes' eq 'Yes'}checked="checked"{/if}  />
        <img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="jobFaultCode_UseLookup_Help" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" >
        </span>
    </p>
    <p>
        <label class="cardLabel" for="ForceLookup" >{$page['Text']['forcelookup_text']|escape:'html'}</label>
        &nbsp;
        <span class="formRadioCheckText"  class="saFormSpan">
        <input type="checkbox" name="ForceLookup" id="UseLookup"  value="Yes" {if $datarow.ForceLookup eq 'Yes'}checked="checked"{/if}  />
        <img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="jobFaultCode_ForceLookup_Help" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" >
        </span>
    </p>
    {if $datarow.JobFaultCodeID !=''}
    <p>
        <label class="cardLabel" for="LinkedItem">{$page['Text']['jobfaultcodelookups_text']|escape:'html'}</label>
        &nbsp;
        <select name="JobFaultCodeLookup[]" id="JobFaultCodeLookupID" class="text" multiple='multiple' style='width:90px;' class='text auto-hint'  >
           {*<option value="" >{$page['Text']['select_default_option']|escape:'html'}</option> *}
            {foreach $LookupList|default:'' as $s}
                
                <option value="{$s.JobFaultCodeLookupID|default:''}" selected="selected" >{$s.LookupName|default:''} </option>
         
            {/foreach}
        </select>
        {*<img style="cursor:pointer" src="{$_subdomain}/images/add_icon.png" onclick="addLinkedItem()"> *}
    </p>
    <p id="addLinkedItemP">
        <div style="text-align:center" id="ajaxLoader"><img src="{$_subdomain}/images/ajax-loading_medium.gif"></div>
    </p>
    {else}
        <p class="saFormSpan" style="text-align: center;">
            <b>{$page['Text']['joblookupinfo_text']|escape:'html'}</b>
        </p>
    {/if}
    {if $datarow.JobFaultCodeID !=''}
        <script>
            var ItemGroup="jobpartscoudelookups";
            function addLinkedItem(){
                $('#addLinkedItemP').html('');
                $('#ajaxLoader').show();
                 $.post("{$_subdomain}/LookupTables/addLinkedItem/",{ PID:{$datarow.JobFaultCodeID|default:''} ,CID:$('#LinkedItem').val(),ItemGroup:ItemGroup},
                function(data) {
                    removeLinkedItemOption($('#LinkedItem').val());
                    loadLinkedItem();
                });
            }
            function removeLinkedItemOption(val){
                $("#LinkedItem option[value="+val+"]").remove();
            }
            function loadLinkedItem(){
                $('#ajaxLoader').show();
                $('#addLinkedItemP').html('');
                $.post("{$_subdomain}/LookupTables/loadLinkedItem/",{ PID:{$datarow.JobFaultCodeID|default:''} ,CID:$('#LinkedItem').val(),ItemGroup:ItemGroup},
                function(data) {
                    en=jQuery.parseJSON(data);
                    $.each(en, function(key, value) { 
                    removeLinkedItemOption(en[key]['ChildItemID'])
                    $('#addLinkedItemP').append("<label class='cardLabel'></label><label title='Click to Remove' class='saFormSpan' style='cursor:pointer' onclick='removeAssoc("+en[key]['JobFaultCodeLookupID']+")'>"+en[key]['ItemName']+"</label><br>");
                    });
                    $('#ajaxLoader').hide();
                });
            }
            function removeAssoc(id){
                if (confirm('Are you sure you want to remove this item?')) {
                    $('#ajaxLoader').show();
                    $.post("{$_subdomain}/LookupTables/delLinkedItem/",{ PID:{$datarow.JobFaultCodeID|default:''} ,CID:id,ItemGroup:ItemGroup},
                    function() {
                        loadLinkedItem();
                    });
                }
            }
        </script>
    {/if}
    </div>
    <hr>
    <div style="height:20px; margin-bottom: 10px;text-align: center;">
        {if $datarow.JobFaultCodeID != '' && $datarow.JobFaultCodeID != '0'}
            <input type="submit" name="update_save_btn" class="textSubmitButton centerBtn" id="update_save_btn" value="{$page['Buttons']['save']|escape:'html'}" />
        {else}
           <input type="submit" name="insert_save_btn" class="textSubmitButton centerBtn" id="insert_save_btn" value="{$page['Buttons']['save']|escape:'html'}" />
        {/if}
        <br/>
        <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;" style="float:right; margin-left: 20px;" value="{$page['Buttons']['cancel']|escape:'html'}" />
    </div>
    </fieldset>
    </form>        
</div>
{/if}

{* This block of code is for to display message after data updation *} 
<div id="dataUpdatedMsg" class="SystemAdminFormPanel" style="display: none;">
    <form id="dataUpdatedMsgForm" name="dataUpdatedMsgForm" method="post" class="inline">  
	<fieldset>
	    <legend title="" > {$form_legend|escape:'html'} </legend>
	    <p><b>{$page['Text']['data_updated_msg']|escape:'html'}</b><br><br></p>
	    <p>
		<span class= "bottomButtons" >
		    <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="location.reload();"  value="{$page['Buttons']['ok']|escape:'html'}" />
		</span>
	    </p>
	</fieldset>		
    </form>
</div>

{* This block of code is for to display message after data insertion *}     
<div id="dataInsertedMsg" class="SystemAdminFormPanel" style="display: none;" >
    <form id="dataInsertedMsgForm" name="dataInsertedMsgForm" method="post" class="inline">
	<fieldset>
	    <legend title="" > {$form_legend|escape:'html'} </legend>
	    <p><b>{$page['Text']['data_inserted_msg']|escape:'html'}</b><br><br></p>
	    <p>
		<span class= "bottomButtons" >
		    <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="location.reload();"  value="{$page['Buttons']['ok']|escape:'html'}" >
		</span>
	    </p>
	</fieldset>   
    </form>
</div>

{* This block of code is for to display message after data insertion *}     
<div id="dataDeletedMsg" class="SystemAdminFormPanel" style="display: none;" >
    <form id="dataDeletedMsgForm" name="dataDeletedMsgForm" method="post" class="inline">
	<fieldset>
	    <legend title="" > {$form_legend|escape:'html'} </legend>
	    <p><b>{$page['Text']['data_deleted_msg']|escape:'html'}</b><br><br></p>
	    <p>
		<span class= "bottomButtons" >
		    <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="location.reload();"  value="{$page['Buttons']['ok']|escape:'html'}" >
		</span>
	    </p>
	</fieldset>   
    </form>
</div>