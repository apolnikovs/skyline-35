<link rel="Stylesheet" type="text/css" href="{$_subdomain}/css/themes/pccs/jPicker-1.1.6.css" />
     <script src="{$_subdomain}/js/jpicker-1.1.6.js" type="text/javascript"></script> 
<script type="text/javascript">   
        $.fn.jPicker.defaults.images.clientPath = "{$_subdomain}/css/themes/pccs/images/colorPicker/";
 $(document).ready(function() {

   $('#Colour').jPicker( {
          window:
          {
            expandable: true,
			position:
			{
			  x: 'screenCenter', // acceptable values "left", "center", "right", "screenCenter", or relative px value
			  y: '0px' // acceptable values "top", "bottom", "center", or relative px value
			}
          }
        }
     
    );
   

 $('.helpTextIconQtip').each(function()
    {
        $HelpTextCode =  $(this).attr("id");
        // We make use of the .each() loop to gain access to each element via the "this" keyword...
        $(this).qtip(
        {
            hide: 'click',
            events: {
         
                hide: function(){
               
                  $(this).qtip('api').set('content.text', '<img src="{$_subdomain}/images/ajax-loader.gif" '); // Direct API method 
                }
            },
            content: {
                // Set the text to an image HTML string with the correct src URL to the loading image you want to use
                text: '<img src="{$_subdomain}/images/ajax-loader.gif" >',
                ajax: {
                    url: '{$_subdomain}/Popup/helpText/' + urlencode($HelpTextCode) + '/Qtip=1/' + Math.random(),
                    once: false // Re-fetch the content each time I'm shown
                },
                title: {
                    text: 'Help', // Give the tooltip a title using each elements text
                    button: true
                }
            },
            position: {
                at: 'bottom center', // Position the tooltip above the link
                my: 'top center',
                viewport: $(window), // Keep the tooltip on-screen at all times
                effect: false // Disable positioning animation
            },
            show: {
                event: 'click',
               
                solo: true // Only show one tooltip at a time
            },
             
            style: {
                classes: 'qtip-tipped  qtip-shadow'
            }
        })
    })
 
    // Make sure it doesn't follow the link when we click it
    .click(function(event) {
        event.preventDefault()
    });   
    
   }); 
   
  </script>  
    
    <div id="PartStatusColourFormPanel" class="SystemAdminFormPanel" >
    
                <form id="PartStatusColourForm" name="PartStatusColourForm" method="post"  action="{$_subdomain}/LookupTables/savePartStatusColour" class="inline" >
                    <input type="hidden" name="SpPartStatusColourID" value="{$datarow.SpPartStatusColourID|default:""}">
                    <input type="hidden" name="PartStatusID" value="{$datarow.PartStatusID|default:""}">
                <fieldset>
                    <legend title="" >Part Status Colour</legend>
                        
                 
                            
                        
                        
                        
 
          <p>
                            <label ></label>
                            <span class="topText" >Fields marked with <span>*</span> are compulsory</span>

          </p>
          <div id="tabs-1" class="SystemAdminFormPanel inline">
              
          <p>
             
                                <label class="cardLabel" for="Colour" >Colour:<sup>*</sup></label>
                                &nbsp;&nbsp;
                                <input  type="text" class="text"  name="Colour" value="{$datarow.Colour|default:''}" id="Colour" >
          </p>
          <p>
                <label class="cardLabel" for="TextColour" >Text Colour:<sup>*</sup></label>
                <input {if $datarow.TextColour|default:'Black'=="Black"}checked=checked{/if} type="radio" name="TextColour" value="Black">Black
                <input {if $datarow.TextColour|default:'Black'=="White"}checked=checked{/if} type="radio" name="TextColour" value="White">White
          </p>
          
          
          
         
          
          
          
          
          </div>
          
   
      
          
  
  <hr>
                               
                                <div style="height:20px;margin-bottom: 10px;text-align: center;">
                                <button type="submit" style="margin-left:38px" class="gplus-blue centerBtn">Save</button>
                                <button type="button" onclick="$.colorbox.close();"  class="gplus-blue" style="float:right">Cancel</button>
                                </div>

                </fieldset>    
                        
                </form>        
                        
       
</div>
                 
 
                          
                        
