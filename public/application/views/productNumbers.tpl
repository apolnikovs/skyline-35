{extends "DemoLayout.tpl"}


    {block name=config}
    {$Title = $page['Text']['page_title']|escape:'html'}
    {$PageId = $ProductNumbers}
    {/block}
    {block name=afterJqueryUI}
        <script type="text/javascript" src="{$_subdomain}/js/jquery.combobox.js"></script>
        <link rel="stylesheet" href="{$_subdomain}/css/themes/pccs/style.css" type="text/css" />
        <style type="text/css" >
            .ui-combobox-input {
                 width:300px;
             }  
        </style>
    {/block}
    {block name=scripts}



    {*<link rel="stylesheet" href="{$_subdomain}/css/colorbox/colorbox.css" type="text/css" charset="utf-8" />*} 
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.jlabel-1.3.min.js"></script>*} 
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.form.min.js"></script>*}
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.validate.min.js"></script>*}
    {*<script type="text/javascript" src="{$_subdomain}/js/additional-methods.min.js"></script>*}
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.dataTables.min.js"></script>*}
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.colorbox-min.js"></script>*} 
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.dataTablesPCCS.js"></script>*}
    
    
    {* These files for autocomplete functioanlity *}
    <script type="text/javascript" >
        var $autoCompleteTag = "#ModelNumber";
    </script>
    
    <script type="text/javascript" src="{$_subdomain}/js/jquery.autocomplete.js"></script>
   
    


    <script type="text/javascript">
    
     var $statuses = [
                    {foreach from=$statuses item=st}
                       ["{$st.Name}", "{$st.Code}"],
                    {/foreach}
                    ]; 
     
     
               
    
                    
        
    function gotoEditPage($sRow)
    {
        
         
        $('#updateButtonId').removeAttr('disabled').removeClass('gplus-blue-disabled').addClass('gplus-blue');
        $('#updateButtonId').trigger('click');
       
       
    }

    /**
    *  This is used to change color of the row if its in-active.
    */
    function inactiveRow(nRow, aData)
    {
          
        if (aData[5]==$statuses[1][1])
        {  
            $(nRow).addClass("inactive");

            $('td:eq(4)', nRow).html( $statuses[1][0] );
        }
        else
        {
            $(nRow).addClass("");
            $('td:eq(4)', nRow).html( $statuses[0][0] );
        }
        
        
        
    }
    
    
    
   
   

    $(document).ready(function() {
        $("#nId").combobox({
            change: function() {
                $(location).attr('href', '{$_subdomain}/ProductSetup/productNumbers/'+urlencode($("#nId").val())+"/"+urlencode($("#cId").val())+'/');
            }
        });
        $("#cId").combobox({
            change: function() {
                $(location).attr('href', '{$_subdomain}/ProductSetup/productNumbers/'+urlencode($("#nId").val())+"/"+urlencode($("#cId").val())+'/');
            }
        });
                  
                   

                   //Click handler for finish button.
                    $(document).on('click', '#finish_btn', 
                                function() {
                                
                                     $(location).attr('href', '{$_subdomain}/SystemAdmin/index/productSetup');

                                });


                      /* Add a change handler to the network dropdown - strats here*/
                        /*$(document).on('change', '#nId', 
                            function() {
                                                           
                                $(location).attr('href', '{$_subdomain}/ProductSetup/productNumbers/'+urlencode($("#nId").val())+"/"+urlencode($("#cId").val())+'/'); 
                            }      
                        );*/
                      /* Add a change handler to the network dropdown - ends here*/
                      
                      
                      
                      
                     /* Add a change handler to the client dropdown - strats here*/
                        /*$(document).on('change', '#cId', 
                            function() {
                                
                                $(location).attr('href', '{$_subdomain}/ProductSetup/productNumbers/'+urlencode($("#nId").val())+"/"+urlencode($("#cId").val())+'/'); 
                            }      
                        );*/
                      /* Add a change handler to the client dropdown - ends here*/ 
                      
                      
                      
                      
                      //change handler for network dropdown box on update/insert popup page.
                      /*$(document).on('change', '#NetworkID', 
                                function() {
                                    
                                   $CompletionStatusDropDownList = $ManufacturerDropDownList =  $clientDropDownList = '<option value="">{$page['Text']['select_default_option']|escape:'html'}</option>';
                                    
                                
                                    var $NetworkID = $("#NetworkID").val();
                                
                                    if($NetworkID && $NetworkID!='')
                                        {
                                           
                                            //Getting clients of network
                                            $.post("{$_subdomain}/ProductSetup/productNumbers/getClients/"+urlencode($NetworkID)+"/",        

                                            '',      
                                            function(data){
                                                    var $networkClients = eval("(" + data + ")");

                                                    if($networkClients)
                                                    {
                                                        for(var $i=0;$i<$networkClients.length;$i++)
                                                        {

                                                            $clientDropDownList += '<option value="'+$networkClients[$i]['ClientID']+'" >'+$networkClients[$i]['ClientName']+'</option>';
                                                        }
                                                    }

                                                    $("#ClientID").html($clientDropDownList);

                                            });
                                  
                                            
                                             //Getting manufacturers of network
                                            $.post("{$_subdomain}/ProductSetup/productNumbers/getManufacturers/"+urlencode($NetworkID)+"/",        

                                            '',      
                                            function(data){
                                                    var $networkManufacturers = eval("(" + data + ")");

                                                    if($networkManufacturers)
                                                    {
                                                        for(var $i=0;$i<$networkManufacturers.length;$i++)
                                                        {

                                                            $ManufacturerDropDownList += '<option value="'+$networkManufacturers[$i]['ManufacturerID']+'" >'+$networkManufacturers[$i]['ManufacturerName']+'</option>';
                                                        }
                                                    }

                                                    $("#ManufacturerID").html($ManufacturerDropDownList);

                                            });
                                                
                                                
                                                
                                                
                                        }
                                    

                                });*/ 
                      
                      
                      
                      
                    //change handler for client dropdown box on update/insert popup page.
                      /*$(document).on('change', '#ClientID', 
                                function() {
                                    
                                    $utDropDownList = '<option value="">{$page['Text']['select_default_option']|escape:'html'}</option>';
                                
                                    var $ClientID = $("#ClientID").val();
                                
                                    if($ClientID && $ClientID!='')
                                        {
                                           
                                            
                                            $.post("{$_subdomain}/ProductSetup/productNumbers/getUnitTypes/"+urlencode($ClientID)+"/",        

                                            '',      
                                            function(data){
                                                    var $clientUnitTypes = eval("(" + data + ")");

                                                    if($clientUnitTypes)
                                                    {
                                                        for(var $i=0;$i<$clientUnitTypes.length;$i++)
                                                        {

                                                            $utDropDownList += '<option value="'+$clientUnitTypes[$i]['UnitTypeID']+'" >'+$clientUnitTypes[$i]['UnitTypeName']+'</option>';
                                                        }
                                                    }

                                                    $("#UnitTypeID").html($utDropDownList);

                                            });
                                  
                                                
                                        }
                                    

                                });*/   
                      
                      
                      
                      
                      //Keyup handler for ModelNumber box on update/insert popup page.
                      $(document).on('blur', '#ModelNumber', 
                                function() {
                                    
                                    var $ModelNumber = $("#ModelNumber").val();
                                
                                    if($ModelNumber && $ModelNumber!='')
                                        {
                                           
                                            
                                            $.post("{$_subdomain}/ProductSetup/productNumbers/getModelDetails/"+urlencode($("#NetworkID").val())+"/"+urlencode($("#ManufacturerID").val())+'/'+urlencode($ModelNumber)+"/",        

                                            '',      
                                            function(data){
                                            
                                                    var $modelDetails = eval("(" + data + ")");

                                                     if($modelDetails['ModelID'])
                                                     {
                                                         
                                                         $("#ManufacturerID").val($modelDetails['ManufacturerID']);
                                                         
                                                         if($("#ManufacturerID").val())
                                                         {
                                                              $("#ManufacturerID").attr("disabled","disabled").addClass('disabledField');
                                                         }
                                                         
                                                         $("#ModelDescription").val($modelDetails['ModelDescription']);
                                                         if($("#ModelDescription").val())
                                                         {
                                                             $("#ModelDescription").attr("disabled","disabled").addClass('disabledField');
                                                         }
                                                         
                                                         
                                                         $("#UnitTypeID").val($modelDetails['UnitTypeID']);
                                                         
                                                     }
                                                     else
                                                     {
                                                         $("#ManufacturerID").removeAttr("disabled").removeClass('disabledField') ;
                                                         $("#ModelDescription").removeAttr("disabled").removeClass('disabledField') ;
                                                     }
                                            });
                                  
                                                
                                        }
                                    

                                }); 
                      
                      
                      
                      

                    /* =======================================================
                    *
                    * set tab on return for input elements with form submit on auto-submit class...
                    *
                    * ======================================================= */

                    $('input[type=text],input[type=password]').keypress( function( e ) {
                            if (e.which == 13) {
                                $(this).blur();
                                if ($(this).hasClass('auto-submit')) {
                                    $('.auto-hint').each(function() {
                                        $this = $(this);
                                        if ($this.val() == $this.attr('title')) {
                                            $this.val('').removeClass('auto-hint').addClass('auto-hint-hide');
                                            if ($this.hasClass('auto-pwd')) {
                                                $this.prop('type','password');
                                            }
                                        }
                                    } );
                                    $(this).get(0).form.onsubmit();
                                } else {
                                    $next = $(this).attr('tabIndex') + 1;
                                    $('[tabIndex="'+$next+'"]').focus();
                                }
                                return false;
                            }
                        } );        


                        
                    
                    
                    $('#ProductNumbersResults').PCCSDataTable( {
                              "aoColumns": [ 
                                                        /* ProductID */  {  "bVisible":    false },    
                                                        /* ProductNo */   null,
                                                        /* ModelID */   null,
                                                        /* Model Description */   null,
                                                        /* UnitTypeID */   null,
                                                        /* Status */  null
                                                ],
                               
                            displayButtons:  "UA",
                            addButtonId:     'addButtonId',
                            addButtonText:   '{$page['Buttons']['insert']|escape:'html'}',
                            createFormTitle: '{$page['Text']['insert_page_legend']|escape:'html'}',
                            createAppUrl:    '{$_subdomain}/ProductSetup/productNumbers/insert/'+urlencode("{$nId}")+"/"+urlencode("{$cId}")+'/',
                            createDataUrl:   '{$_subdomain}/ProductSetup/ProcessData/ProductNumbers/',
                            createFormFocusElementId:'NetworkID',
                            formInsertButton:'insert_save_btn',
                           
                            frmErrorRules:   {
                                                    NetworkID:
                                                        {
                                                            required: true
                                                        },
                                                    ClientID:
                                                        {
                                                            required: true
                                                        },
                                                    ProductNo:
                                                        {
                                                            required: true
                                                        },
                                                    ModelNumber:
                                                        {
                                                            required: true
                                                        },
                                                    ModelDescription:
                                                        {
                                                            required: true
                                                        },    
                                                    UnitTypeID:
                                                        {
                                                            required: true
                                                        },
                                                    ManufacturerID:
                                                        {
                                                            required: true
                                                        },
                                                    ActualSellingPrice:
                                                        {
                                                            number: true
                                                        },
                                                    AuthorityLimit:
                                                        {
                                                            number: true
                                                        },
                                                    PaymentRoute:
                                                        {
                                                            required: true
                                                        }    
                                                        
                                             },
                                                
                           frmErrorMessages: {
                           
                                                    NetworkID:
                                                        {
                                                           required: "{$page['Errors']['network']|escape:'html'}"
                                                        },
                                                    ClientID:
                                                        {
                                                            required: "{$page['Errors']['client']|escape:'html'}"
                                                        },
                                                    ProductNo:
                                                        {
                                                            required: "{$page['Errors']['product_number']|escape:'html'}"
                                                        },
                                                    ModelNumber:
                                                        {
                                                            required: "{$page['Errors']['model']|escape:'html'}"
                                                        },
                                                    ModelDescription:
                                                        {
                                                            required: "{$page['Errors']['description']|escape:'html'}"
                                                        },    
                                                        
                                                    UnitTypeID:
                                                        {
                                                            required: "{$page['Errors']['unit_type']|escape:'html'}"
                                                        },
                                                    ManufacturerID:
                                                        {
                                                            required: "{$page['Errors']['manufacturer']|escape:'html'}"
                                                        },
                                                    ActualSellingPrice:
                                                        {
                                                            number: "{$page['Errors']['price_format']|escape:'html'}"
                                                        },
                                                    AuthorityLimit:
                                                        {
                                                            number: "{$page['Errors']['price_format']|escape:'html'}"
                                                        },
                                                    PaymentRoute:
                                                        {
                                                            required: "{$page['Errors']['payment_route']|escape:'html'}"
                                                        }  
                           
                                                     
                                              },                     
                            
                            popUpFormWidth:  750,
                            popUpFormHeight: 430,
                            
                            
                            updateButtonId:  'updateButtonId',
                            updateButtonText:'{$page['Buttons']['edit']|escape:'html'}',
                            updateFormTitle: '{$page['Text']['update_page_legend']|escape:'html'}',
                            updateAppUrl:    '{$_subdomain}/ProductSetup/productNumbers/update/',
                            updateDataUrl:   '{$_subdomain}/ProductSetup/ProcessData/ProductNumbers/',
                            formUpdateButton:'update_save_btn',
                            updateFormFocusElementId:'NetworkID',
                            
                            colorboxFormId:  "ProductNumbersForm",
                            frmErrorMsgClass:"fieldError",
                            frmErrorElement: "label",
                            htmlTablePageId: 'ProductNumbersResultsPanel',
                            htmlTableId:     'ProductNumbersResults',
                            fetchDataUrl:    '{$_subdomain}/ProductSetup/ProcessData/ProductNumbers/fetch/'+urlencode("{$nId}")+"/"+urlencode("{$cId}"),
                            formCancelButton:'cancel_btn',
                            
                            fnRowCallback:          'inactiveRow',
                            searchCloseImage:'{$_subdomain}/css/Skins/{$_theme}/images/close.png',
                            
                            dblclickCallbackMethod: "gotoEditPage",
                            tooltipTitle:   "{$page['Text']['tooltip_title']|escape:'html'}",
                            
                            iDisplayLength:  25,
                            formDataErrorMsgId: "suggestText",
                            frmErrorSugMsgClass:"formCommonError",
                            sDom: 'ft<"#dataTables_command">rpli',
                            bottomButtonsDivId:'dataTables_command'


                        });
                      

                        
                   

    });

    </script>

    {/block}

    {block name=body}

    <div class="breadcrumb">
        <div>

            <a href="{$_subdomain}/SystemAdmin" >{$page['Text']['system_admin_home_page']|escape:'html'}</a> / <a href="{$_subdomain}/SystemAdmin/index/productSetup" >{$page['Text']['product_setup']|escape:'html'}</a> / {$page['Text']['page_title']|escape:'html'}

        </div>
    </div>



    <div class="main" id="home" >

               <div class="ServiceAdminTopPanel" >
                    <form id="ProductNumbersTopForm" name="ProductNumbersTopForm" method="post"  action="#" class="inline">

                        <fieldset>
                        <legend title="" >{$page['Text']['legend']|escape:'html'}</legend>
                        <p>
                            <label>{$page['Text']['description']|escape:'html'}</label>
                        </p> 

                        </fieldset> 


                    </form>
                </div>  

                        
                <div class="ServiceAdminResultsPanel" id="ProductNumbersResultsPanel" >
                   <form name="listsForm" id="productNumbersListsForm">  
                    
                    {if $SupderAdmin eq true} 
                        {$page['Labels']['service_network_label']|escape:'html'}
                        <select name="nId" id="nId"  >
                            <option value="" {if $nId eq ''}selected="selected"{/if}>{$page['Text']['select_service_network']|escape:'html'}</option>

                            {foreach $networks as $network}

                                <option value="{$network.NetworkID}" {if $nId eq $network.NetworkID}selected="selected"{/if}>{$network.CompanyName|escape:'html'}</option>

                            {/foreach}
                        </select>
                        <br /><span style="padding-right: 59px;">{$page['Labels']['client_label']|escape:'html'}</span>
                        <select name="cId" id="cId"  >
                            <option value="" {if $cId eq ''}selected="selected"{/if}>{$page['Text']['select_client']|escape:'html'}</option>

                            {foreach $clients as $client}

                                <option value="{$client.ClientID}" {if $cId eq $client.ClientID}selected="selected"{/if}>{$client.ClientName|escape:'html'}</option>

                            {/foreach}
                        </select>
                        
                        
                    {else if $NetworkUser eq true}
                        
                        
                        <input type="hidden" name="nId" id="nId" value="{$nId|escape:'html'}" >   
                        {$page['Labels']['client_label']|escape:'html'}
                        <select name="cId" id="cId"  >
                            <option value="" {if $cId eq ''}selected="selected"{/if}>{$page['Text']['select_client']|escape:'html'}</option>

                            {foreach $clients as $client}

                                <option value="{$client.ClientID}" {if $cId eq $client.ClientID}selected="selected"{/if}>{$client.ClientName|escape:'html'}</option>

                            {/foreach}
                        </select>
                        
                        
                    {else if $ClientUser eq true}
                        
                        <input type="hidden" name="nId" id="nId" value="{$nId|escape:'html'}" > 
                        <input type="hidden" name="cId" id="cId" value="{$cId|escape:'html'}" >  
                       
                    {/if} 
                    
                     </form>
                    
                    
                    
                    <form id="ProductNumbersResultsForm" class="dataTableCorrections">
                        <table id="ProductNumbersResults" border="0" cellpadding="0" cellspacing="0" class="browse" >
                            <thead>
                                    <tr>
                                            <th></th> 
                                            <th title="{$page['Text']['product_number']|escape:'html'}" >{$page['Text']['product_number']|escape:'html'}</th>
                                            <th title="{$page['Text']['model_number']|escape:'html'}" >{$page['Text']['model_number']|escape:'html'}</th>
                                            <th title="{$page['Text']['model_description']|escape:'html'}" >{$page['Text']['model_description']|escape:'html'}</th>
                                            <th title="{$page['Text']['unit_type']|escape:'html'}" >{$page['Text']['unit_type']|escape:'html'}</th>
                                            <th title="{$page['Text']['status']|escape:'html'}"  >{$page['Text']['status']|escape:'html'}</th>
                                            
                                    </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>  
                     </form>
                </div>        

                <div class="bottomButtonsPanel" >
                    <hr>
                    
                    <button id="finish_btn" type="button" class="gplus-blue rightBtn" ><span class="label">{$page['Buttons']['finish']|escape:'html'}</span></button>
                    
                </div>        
               
    </div>
                        
                        



{/block}



