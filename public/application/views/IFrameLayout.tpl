<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>

    {* JQuery *}
    <script type="text/javascript" src="{$_subdomain}/js/jquery-1.8.2.min.js"></script> 

    {* Base CSS *}
    <link rel="stylesheet" href="{$_subdomain}/css/base_css.php?size=950" type="text/css" media="screen" charset="utf-8" />

    {* Skin CSS *}
    <link rel="stylesheet" href="{$_subdomain}/css/skin_css.php?skin={$_theme}&size=950" type="text/css" media="screen" charset="utf-8" />

    {* IE only styles *}
    <!--[if lte IE 8]><link rel="stylesheet" href="{$_subdomain}/css/Base/lessblue/ie.css" type="text/css" media="screen" /><![endif]-->
    <!--[if IE 8]>
    <link rel="stylesheet" type="text/css" href="{$_subdomain}/css/Base/lessblue/ie8.css">
    <![endif]-->

    {block name=scripts}{/block} 

</head>

<body>

    <div class="container">
    {block name=body}{/block}
    </div>

<!--[if IE 6]>
<script type="text/javascript" src="{$_subdomain}/js/DD_belatedPNG_0.0.8a.js"></script>
<script>
$(document).ready(function() {
    DD_belatedPNG.fix('img, .node');
});//end document ready function
</script>
<![endif]--> 

{* ***********************************************************************
    
IMPORTANT NOTE: The closing body and html tags are now appended to the output stream
in index.php.

</body>
            
</html>

**************************************************************************** *}