<script type="text/javascript">
    $(document).ready(function() {
        $("#BrandID").combobox({
            change: function() {
                $jobTypesDropDownList = '<option value="">{$page['Text']['select_default_option']|escape:'html'}</option>';        
                var $brandId = $("#BrandID").val();
                if($brandId && $brandId!='')
                {
                    $brandJobTypes = $jobTypes[$brandId];
                    if($brandJobTypes)
                    {
                        for(var $i=0;$i<$brandJobTypes.length;$i++)
                        {
                            $jobTypesDropDownList += '<option value="'+$brandJobTypes[$i][0]+'" >'+$brandJobTypes[$i][1]+'</option>';
                        }
                    }
                   $("#JobTypeID").html($jobTypesDropDownList);     
                }
            }
        });
        $("#JobTypeID").combobox();
    });
</script>
{if $accessErrorFlag eq true} 
    
    <div id="accessDeniedMsg" class="SystemAdminFormPanel"  >   
    
        <form id="accessDeniedMsgForm" name="accessDeniedMsgForm" method="post"  action="#" class="inline">
                <fieldset>
                    <legend title="" > {$page['Text']['error_page_legend']|escape:'html'} </legend>
                    <p>

                        <label class="formCommonError" >{$accessDeniedMsg|escape:'html'}</label><br><br>

                    </p>
                    
                    <p>

                        <span class= "bottomButtons" >


                            <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['ok']|escape:'html'}" >

                        </span>

                    </p>


                </fieldset>   
        </form>            


    </div>    
    
{else}  
    
    <div id="serviceTypesFormPanel" class="SystemAdminFormPanel" >
    
                <form id="STForm" name="STForm" method="post"  action="#" class="inline">
       
                <fieldset>
                    <legend title="" >{$form_legend|escape:'html'}</legend>
                        
                    <p><label id="suggestText" ></label></p>
                            <p>
                            <label ></label>
                            <span class="topText" >{$page['Text']['top_info_text1']|escape:'html'} <span>*</span> {$page['Text']['top_info_text2']|escape:'html'}</span>

                            </p>
                       
        
                         <p>
                            <label class="fieldLabel" for="BrandID" >{$page['Labels']['brand']|escape:'html'}:<sup>*</sup></label>
                           
                            &nbsp;&nbsp;
                            <select name="BrandID" id="BrandID" >
                               
                                <option value="" {if $datarow.BrandID eq ''}selected="selected"{/if}>{$page['Text']['select_default_option']|escape:'html'}</option>

                                {foreach $brands as $brand}

                                    <option value="{$brand.BrandID|escape:'html'}" {if $datarow.BrandID eq $brand.BrandID} selected="selected" {/if}>{$brand.Name|escape:'html'} ({$brand.BrandID|escape:'html'}) </option>

                                {/foreach}

                            </select>
                        
                         </p>
                        
                         
                          <p>
                            <label class="fieldLabel" for="JobTypeID" >{$page['Labels']['job_type']|escape:'html'}:<sup>*</sup></label>
                           
                             &nbsp;&nbsp; 
                             <select name="JobTypeID" id="JobTypeID" >
                               
                                <option value="" {if $datarow.JobTypeID eq ''}selected="selected"{/if}>{$page['Text']['select_default_option']|escape:'html'}</option>

                                {foreach $jobTypes as $jobType}

                                    <option value="{$jobType.JobTypeID|escape:'html'}" {if $datarow.JobTypeID eq $jobType.JobTypeID} selected="selected" {/if}>{$jobType.Type|escape:'html'}</option>

                                {/foreach}

                            </select>
                         </p>
                         
                       
                         
                         <p>
                            <label class="fieldLabel" for="ServiceTypeName" >{$page['Labels']['name']|escape:'html'}:<sup>*</sup></label>
                           
                             &nbsp;&nbsp; <input  type="text" class="text"  name="ServiceTypeName" value="{$datarow.ServiceTypeName|escape:'html'}" id="ServiceTypeName" >
                        
                         </p>
                         
                         
                         
                          <p>
                            <label class="fieldLabel" for="Code" >{$page['Labels']['code']|escape:'html'}:<sup>*</sup></label>
                           
                             &nbsp;&nbsp; <input  type="text" class="text"  name="Code" value="{$datarow.Code|escape:'html'}" id="Code" >
                        
                         </p>
                       
                         
                         <p>
                            <label class="fieldLabel" for="Priority" >{$page['Labels']['priority']|escape:'html'}:<sup>*</sup></label>
                           
                             &nbsp;&nbsp; <input  type="text" class="text"  name="Priority" value="{$datarow.Priority|escape:'html'}" id="Priority" >
                        
                         </p>
                          

                          <p>
                            <label class="fieldLabel" for="Status" >{$page['Labels']['status']|escape:'html'}:<sup>*</sup></label>
                           
                             &nbsp;&nbsp; 
                        
                                {foreach $statuses as $status}

                                    <input  type="radio" name="Status"  value="{$status.Code}" {if $datarow.Status eq $status.Code} checked="checked" {/if}  /> <span class="text" >{$status.Name|escape:'html'}</span> 

                                {/foreach}    



                          </p>
                         
                         

                            <p>
                    
                                <span class= "bottomButtons" >

                                    <input type="hidden" name="ServiceTypeID"  value="{$datarow.ServiceTypeID|escape:'html'}" >
                                    <input type="hidden" name="ServiceTypeCode"  value="{$datarow.ServiceTypeCode|escape:'html'}" >

                                    {if $datarow.ServiceTypeID neq '' && $datarow.ServiceTypeID neq '0'}
                                        
                                         <input type="submit" name="update_save_btn" class="textSubmitButton centerBtn" id="update_save_btn"  value="{$page['Buttons']['save']|escape:'html'}" >
                                    
                                    {else}
                                        
                                        <input type="submit" name="insert_save_btn" class="textSubmitButton centerBtn" id="insert_save_btn"  value="{$page['Buttons']['save']|escape:'html'}" >
                                        
                                    {/if}
                                   
                                        <br>
                                        <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['cancel']|escape:'html'}" >



                                </span>

                            </p>

                           
                          
                         
                         


                </fieldset>    
                        
                </form>        
                        
       
</div>
                 
{/if}  
                                    
                                    
{* This block of code is for to display message after data updation *} 

                                    
<div id="dataUpdatedMsg" class="SystemAdminFormPanel" style="display: none;" >   
    
    <form id="dataUpdatedMsgForm" name="dataUpdatedMsgForm" method="post"  action="#" class="inline">
            <fieldset>
                <legend title="" > {$form_legend|escape:'html'} </legend>
                <p>
                 
                    <b>{$page['Text']['data_updated_msg']|escape:'html'}</b><br><br>
                 
                </p>
                
                <p>

                    <span class= "bottomButtons" >


                        <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['ok']|escape:'html'}" >

                    </span>

                </p>
                          
                
            </fieldset>   
    </form>            
    
    
</div>    

    
 
  {* This block of code is for to display message after data insertion *}                      
                        
<div id="dataInsertedMsg" class="SystemAdminFormPanel" style="display: none;" >   
    
    <form id="dataInsertedMsgForm" name="dataInsertedMsgForm" method="post"  action="#" class="inline">
            <fieldset>
                <legend title="" > {$form_legend|escape:'html'} </legend>
                <p>
                 
                    <b>{$page['Text']['data_inserted_msg']|escape:'html'}</b><br><br>
                 
                </p>
                
                <p>

                    <span class= "bottomButtons" >


                        <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['ok']|escape:'html'}" >

                    </span>

                </p>
                          
                
            </fieldset>   
    </form>            
    
    
</div>                            
                        
