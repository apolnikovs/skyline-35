<?php

/**
 * Short Description of CustomRestController.
 * 
 * Long description of CustomRestController.
 *
 * @author     Brian Etherington <brian@smokingun.co.uk>
 * @copyright  2011 Smokingun Graphics
 * @link       http://www.smokingun.co.uk
 * @version    1.1
 *  
 * Changes
 * Date        Version Author                Reason
 * 23/01/2012  1.0     Brian Etherington     Initial Version
 * ??/??/2012  1.1     Brian Etherington  
 ******************************************************************************/

require_once('CustomController.class.php');

abstract class CustomRESTController extends CustomController {
    
    // beginning of docblock template area
    /**#@+
     * @access protected
     */
    // protected  vars here....
    /**#@-*/
    
    // beginning of docblock template area
    /**#@+
     * @access private
     */
    private $request_vars;
    private $http_accept;
    private $method;
    private $config;
    
    private $codes = Array(
		    100 => 'Continue',
		    101 => 'Switching Protocols',
		    200 => 'OK',
		    201 => 'Created',
		    202 => 'Accepted',
		    203 => 'Non-Authoritative Information',
		    204 => 'No Content',
		    205 => 'Reset Content',
		    206 => 'Partial Content',
		    300 => 'Multiple Choices',
		    301 => 'Moved Permanently',
		    302 => 'Found',
		    303 => 'See Other',
		    304 => 'Not Modified',
		    305 => 'Use Proxy',
		    306 => '(Unused)',
		    307 => 'Temporary Redirect',
		    400 => 'Bad Request',
		    401 => 'Unauthorized',
		    402 => 'Payment Required',
		    403 => 'Forbidden',
		    404 => 'Not Found',
		    405 => 'Method Not Allowed',
		    406 => 'Not Acceptable',
		    407 => 'Proxy Authentication Required',
		    408 => 'Request Timeout',
		    409 => 'Conflict',
		    410 => 'Gone',
		    411 => 'Length Required',
		    412 => 'Precondition Failed',
		    413 => 'Request Entity Too Large',
		    414 => 'Request-URI Too Long',
		    415 => 'Unsupported Media Type',
		    416 => 'Requested Range Not Satisfiable',
		    417 => 'Expectation Failed',
		    500 => 'Internal Server Error',
		    501 => 'Not Implemented',
		    502 => 'Bad Gateway',
		    503 => 'Service Unavailable',
		    504 => 'Gateway Timeout',
		    505 => 'HTTP Version Not Supported'
            );
    
    /**#@-*/
    
    public function __construct($config = 'api.ini') { 
        
        parent::__construct();
        
        $this->request_vars = array();
	$this->http_accept  = (stripos($_SERVER['HTTP_ACCEPT'], 'json') !== false) ? 'json' : 'xml';
	$this->method	    = strtolower($_SERVER['REQUEST_METHOD']);
        $this->config = $this->readConfig($config);
        
    }
    
    // Define abstract methods as protected (not public) so they can be hidden 
    // in API Controller class.
    abstract protected function get( $args );
    abstract protected function post( $args );
    abstract protected function put( $args );
    abstract protected function delete( $args );
    
    /**
     * Return user's password as md5 encoded string;
     * 
     * Override this function in derived api class to return the users password.
     * If username not found return null.
     * 
     * @param string $user
     * $return string $password
     */
    protected function getPassword($user) {     
	return null;
    }
            
    /**
     * Short Description.
     * 
     * Description.
     * 
     * @param array $args
     */
    public function indexAction( /* $args */ ) {
               
        switch ($this->method) {
            // gets are easy...
            case 'get':
                $this->get( $_GET );
		break;
            // so are posts
            case 'post':
                $this->post( $_POST );
		break;
            // here's the tricky bit...
            case 'put':
                // basically, we read a string from PHP's special input location,
		// and then parse it out into an array via parse_str... per the PHP docs:
		// Parses str  as if it were the query string passed via a URL and sets
		// variables in the current scope.
		parse_str(file_get_contents('php://input'), $this->request_vars);
                $this->put( $this->request_vars );
		break;
            case 'delete':
		// basically, we read a string from PHP's special input location,
		// and then parse it out into an array via parse_str... per the PHP docs:
		// Parses str  as if it were the query string passed via a URL and sets
		// variables in the current scope.
		parse_str(file_get_contents('php://input'), $this->request_vars);
                $this->delete( $this->request_vars );
		break;
            default:
                throw new Exception('Unrecognised method: '.$this->method);
	}                
        
    }
    
    /*protected function authenticate() {
        
        if (!$this->isAuthorised()) {
            header('WWW-Authenticate: Basic realm="'.$this->config['params']['realm'].'"');
            $this->sendResponse(401);
            exit;
        } 

	return $_SERVER['PHP_AUTH_USER'];

    }
    
        
    /**
     * Short Description.
     * 
     * Description.
     * 
     * @return string $username
     */
    protected function authenticate() {
        
        if (!$this->isAuthorised()) {
            header('WWW-Authenticate: Basic realm="'.$this->config['params']['realm'].'"');
            $this->sendResponse(401);
            exit;
        } 
        
	return $_SERVER['PHP_AUTH_USER'];

    }
    
    /**
     * Short Description.
     * 
     * Description.
     * 
     * @return boolean $isAuthorised
     */
    protected function isAuthorised() {
       
        if (isset($_SERVER['PHP_AUTH_USER']) && isset($_SERVER['PHP_AUTH_PW'])) {      
       
		$password = isset($this->config['users'][$_SERVER['PHP_AUTH_USER']]) 
                          ? $this->config['users'][$_SERVER['PHP_AUTH_USER']]
                          : $this->getPassword($_SERVER['PHP_AUTH_USER']);
        
        	if ( $password === md5($_SERVER['PHP_AUTH_PW'].'|sKyLiNe|'.strrev($_SERVER['PHP_AUTH_PW'])) ) return true;
	}
        
        return false;
        
    }
       
    protected function sendResponse($status = 200, $body = '') {
        
        $statusCodeMessage = $this->getStatusCodeMessage($status);
        
        $status_header = 'HTTP/1.1 ' . $status . ' ' . $statusCodeMessage;
	// set the status
	header($status_header);
        
        if ( ($status < 200) OR ($status > 202) ) {
            $this->log(get_class($this)."\nStatus: $status\nStatus Message: $statusCodeMessage\nResponse: ".var_export($body,true));
        }

	// we need to create the body if none is passed
	if($body == '') {
            
            // set the content type
            header('Content-type: text/html');

            // create some body messages
            $message = '';

            // this is purely optional, but makes the pages a little nicer to read
            // for your users.  Since you won't likely send a lot of different status codes,
            // this also shouldn't be too ponderous to maintain
            switch($status) {
		case 401:
                    $message = 'You must be authorized to view this page.';
                    break;
                case 404:
                    $message = 'The requested URL ' . $_SERVER['REQUEST_URI'] . ' was not found.';
                    break;
                case 500:
                    $message = 'The server encountered an error processing your request.';
                    break;
                case 501:
                    $message = 'The requested method is not implemented.';
                    break;
            }

            // servers don't always have a signature turned on (this is an apache directive "ServerSignature On")
            $signature = ($_SERVER['SERVER_SIGNATURE'] == '') ? $_SERVER['SERVER_SOFTWARE'] . ' Server at ' . $_SERVER['SERVER_NAME'] . ' Port ' . $_SERVER['SERVER_PORT'] : $_SERVER['SERVER_SIGNATURE'];

            $body = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">'
                  . '<html>'
                  . '<head>'
		  .   '<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">'
		  .   '<title>' . $status . ' ' . $statusCodeMessage . '</title>'
                  . '</head>'
                  . '<body>'
		  .   '<h1>' . $statusCodeMessage . '</h1>'
		  .   '<p>' . $message . '</p>'
		  .   '<hr />'
		  .   '<address>' . $signature . '</address>'
                  . '</body>'
                  . '</html>';
  
        } else {
            
            switch($this->http_accept) {
                case 'json':
                    header('Content-type: application/json');
                    $body = json_encode($body);
                    break;
                case 'xml':
                    header('Content-type: application/xml');
                    $body = '<?xml version="1.0" encoding="UTF-8"?><Response>' 
                          . $this->SerializeAsXML($body)
                          . '</Response>';
                    break;                  
            }
            
        }
        
        echo $body;
        
    }
    
    private function SerializeAsXML($obj, $depth = 0) {
        
        $d = '';
        
        if (is_array($obj) || is_object($obj)) {
            
            $tabs = '';
            
            for ($i = 0; $i <= $depth; $i++) {
                $tabs .= "\t";
            }
            
            foreach ($obj as $o_i => $o_v) {
                
                $useTab = false;
                /* If element is numeric then ignore as xml tags can not be numeric. 
                 * Data being passed should have been through recordSetSerialise when building response */
                if (!is_int($o_i)) {
                    /*Not numeric - so use tag */               
                    $d .= $tabs . "<{$o_i}>";
                }
                if (is_array($o_v) || is_object($o_v)) {
                    $d .= "\n" . $this->SerializeAsXML($o_v, ($depth + 1));
                    $useTab = true;
                } else {
                    //$d .= htmlentities($o_v, ENT_COMPAT);
                    $d .= htmlentities( $o_v, ENT_QUOTES | ENT_XML1 );
                }
                if ($useTab) {
                    $d .= $tabs;
                }
                if (!is_int($o_i)) {
                    /*Not numeric - so use tag */    
                    $d .= "</{$o_i}>\n";
                }
            }
            
        }
        
        return $d;
    }
    
    protected function getStatusCodeMessage($status) {
	return (isset($this->codes[$status])) ? $this->codes[$status] : '';
    }
    
    /**
     * Description
     * 
     * Serialise a recordset so ecah record is under number=>tag for easy
     * xml construction
     * 
     * @param array $recordset     Recordset from DbQuery, 
     *        string $tag          Tag to surround each record        
     * 
     * @return array recordSetSerialise   Associative array containing respose
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     */
    
    public function recordSetSerialise($recordset, $tag) {         
        $response = array();
        
        foreach ($recordset as  $n => $field) {
            $response[$n][$tag] = $field;
        }
        return($response);
    }
 
}

?>
