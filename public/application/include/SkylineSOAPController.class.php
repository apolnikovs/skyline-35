<?php

/**
 * SkylineSoapController.class.php
 * 
 * Implementation of the Generic SOAP Controller for Skyline
 * 
 * This adds skyline authentication to the basic WashiMVC Soap Controller. It 
 * should then be derived for further classes 
 *
 * @author     Andrew Williams <a.williams@pccsuk.com>
 * @copyright  2013 PC Control Systems
 * @link 
 * @version    1.0
 * 
 * Changes
 * Date        Version Author                Reason
 * 23/01/2013  1.00    Andrew J. Williams    Initial Version
 ******************************************************************************/

require_once('CustomSOAPController.class.php');

class SkylineSOAPController extends CustomSOAPController {
    
    private $user_info;

    public function __construct() {
        parent::__construct(); 
        
        $this->user_info = new stdClass;
        $this->serverpath = isset($_SERVER['HTTPS']) ? 'https://' : 'http://';
        $this->serverpath .= $_SERVER['SERVER_NAME'];
        $this->serverpath .= defined('SUB_DOMAIN') ? SUB_DOMAIN : '';
    }
    
    public function authorise($pass) {
        if ( $this->user_info->Password === md5($pass.'|sKyLiNe|'.strrev($pass)) ) {
            return (true);
	} else {
            return (false);
        }
    }
    
    
    /**
     * getUserDetails
     * 
     * Get the skyline details for a username
     * 
     * @param string $user  User name
     * 
     * @return none
     * 
     * @author Andrew Williams <a.williams@pccsuk.com> 
     **************************************************************************/    
    protected function getUserDetails($user) { 
        $user_model = $this->loadModel('Users');
                       
        $this->user_info = $user_model->GetUser($user);
    }
     
    
    /**
     * Description
     * 
     * Get the md5 hash password associated with the SkyLine username
     * 
     * @param none
     * 
     * @return string $getPassword - encrypted hash of users passwod
     * 
     * @author Andrew Williams <a.williams@pccsuk.com> 
     **************************************************************************/    
    protected function getPassword($user) { 
       return($this->user->Password);
    }
    
    
    
}

?>
