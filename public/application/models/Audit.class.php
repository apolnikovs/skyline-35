<?php
require_once('CustomModel.class.php');
require_once('TableFactory.class.php');

/**
 * Description of Customer
 * 
 * This model handles interacttion with the Audit table.
 *
 * @author      Brian etherington <b.etherington@pccsuk.com>
 * 
 * @copyright   2012 PC Control Systems Ltd
 * 
 * Changes
 * Date        Version Author               Reason
 * 05/07/2012  1.00    Brian Etherington    Initial Version      

 ******************************************************************************/

class Audit extends CustomModel{
    
    private $conn;
    private $table;

    public function __construct($controller) {
    
        parent::__construct($controller); 

        $this->conn = $this->Connect( $this->controller->config['DataBase']['Conn'],
                                      $this->controller->config['DataBase']['Username'],
                                      $this->controller->config['DataBase']['Password'] ); 
        
        $this->table = TableFactory::Audit();
  
    }
    
        /**
     * Short Description.
     * 
     * Description.
     * 
     * @param array $args
     * @return array $result
     */
    public function create($args) {
        $result = null;
        $args['CreatedDate'] = null;
        $args['ModifiedUserID'] = $this->controller->user->UserID;
        $cmd = $this->table->insertCommand( $args );
        if ($this->Execute($this->conn, $cmd, $args)) {
            $result =  array('status' => 'OK',
                             'id' => $this->conn->lastInsertId());
        } else {
            $result = array('status' => 'ERROR',
                            'message' => $this->lastPDOError());          
        }
        return $result;
    }
    
     /**
     * Short Description.
     * 
     * Description.
     * 
     * @param array $args
     * @return array $result
     */   
    public function update($args) {
        $result = null;
        // Do not allow update to change the Created Date
        if (isset($args['CreatedDate'])) unset($args['CreatedDate']);
        $args['ModifiedUserID'] = $this->controller->user->UserID;
        $cmd = $this->table->updateCommand( $args );
        if ($this->Execute($this->conn, $cmd, $args)) {
            $result =  array('status' => 'OK');
        } else {
            $result = array('status' => 'ERROR',
                            'message' => $this->lastPDOError());          
        }
        return $result;
    }
    
     /**
     * Short Description.
     * 
     * Description.
     * 
     * @param array $args
     * @return array $result
     */   
    public function delete($args) {
        $result = null;
        $cmd = $this->table->deleteCommand( $args );
        if ($this->Execute($this->conn, $cmd, $args)) {
            $result =  array('status' => 'OK');
        } else {
            $result = array('status' => 'ERROR',
                            'message' => $this->lastPDOError());          
        }
        return $result;
    } 
    
    
    
    /**
    @action get audit trail count by job.JobID for job table
    @input  job ID
    @output void
    @return audit trail count
    */
    
    public function getAuditTrailCountByJobID($jobID) {
	
	$q = "SELECT COUNT(*) AS `count` FROM audit_new WHERE TableName = 'job' AND PrimaryID = :jobID AND FieldName != 'StatusID'";
	$values = ["jobID" => $jobID];
	$result = $this->query($this->conn, $q, $values);
	return (count($result) > 0) ? $result[0]["count"] : false;
	
    }

    
    
    /**
    @action get audit trail data by job.JobID for job table
    @input  job ID
    @output void
    @return audit trail data
    */
    
    public function getAuditTrailByJobID($jobID) {
        
	//changed display date format by thirumal..%d/%m/%y replace with %d/%/m%Y
	$q = "	SELECT	DATE_FORMAT(`Date`, '%d/%m/%Y')				    AS '0',
			DATE_FORMAT(`Date`, '%H:%i')				    AS '1',
			
			(SELECT CONCAT(user.ContactFirstName, ' ', user.ContactLastName) FROM user WHERE UserID = audit_new.UserID) AS '2',

			CASE
			    WHEN FieldName = 'ProductID'		THEN 'Catalogue No'
			    WHEN FieldName = 'ManufacturerID'		THEN 'Manufacturer'
			    WHEN FieldName = 'ServiceBaseUnitType'	THEN 'Product Type'
			    WHEN FieldName = 'RepairType'		THEN 'Job Type'
			    WHEN FieldName = 'ServiceTypeID'		THEN 'Service Type'
			    WHEN FieldName = 'SerialNo'			THEN 'Serial No'
			    WHEN FieldName = 'ProductLocation'		THEN 'Item Location'
			    WHEN FieldName = 'DateOfPurchase'		THEN 'Purchase Date'
			    WHEN FieldName = 'OriginalRetailer'		THEN 'Original Retailer'
			    WHEN FieldName = 'ReceiptNo'		THEN 'Sales Receipt No'
			    WHEN FieldName = 'PolicyNo'			THEN 'Policy No'
			    WHEN FieldName = 'AuthorisationNo'		THEN 'Authorisation No'
			    WHEN FieldName = 'AgentRefNo'		THEN 'Referral No'
			    WHEN FieldName = 'ReportedFault'		THEN 'Issue Report'
			    WHEN FieldName = 'UnitCondition'		THEN 'Condition'
			    WHEN FieldName = 'ServiceBaseModel'		THEN 'Service Base Model'
			    WHEN FieldName = 'ServiceTypeID'		THEN 'Service Type'
			    WHEN FieldName = 'ChargeableLabourCost'	THEN 'Chargeable Labour Cost'
			    WHEN FieldName = 'ChargeablePartsCost'	THEN 'Chargeable Parts Cost'
			    WHEN FieldName = 'ChargeableDeliveryCost'	THEN 'Chargeable Delivery Cost'
			    WHEN FieldName = 'ChargeableDeliveryVATCost' THEN 'Chargeable Delivery VAT Cost'
			    WHEN FieldName = 'ChargeableVATCost'	THEN 'Chargeable VAT Cost'
			    WHEN FieldName = 'ChargeableSubTotal'	THEN 'Chargeable Sub Total'
			    WHEN FieldName = 'ChargeableTotalCost'	THEN 'Chargeable Total Cost'
			    WHEN FieldName = 'ColAddCompanyName'	THEN 'Collection Address Company Name'
			    WHEN FieldName = 'ColAddBuildingNameNumber' THEN 'Collection Address Building Name Number'
			    WHEN FieldName = 'ColAddStreet'		THEN 'Collection Address Street'
			    WHEN FieldName = 'ColAddLocalArea'		THEN 'Collection Address Local Area'
			    WHEN FieldName = 'ColAddTownCity'		THEN 'Collection Address Town City'
			    WHEN FieldName = 'ColAddCountyID'		THEN 'Collection Address County'
			    WHEN FieldName = 'ColAddCountryID'		THEN 'Collection Address Country'
			    WHEN FieldName = 'ColAddPostcode'		THEN 'Collection Address Postcode'
			    WHEN FieldName = 'ColAddEmail'		THEN 'Collection Address Email'
			    WHEN FieldName = 'ColAddPhone'		THEN 'Collection Address Phone'
			    WHEN FieldName = 'ColAddPhoneExt'		THEN 'Collection Address Phone Ext'
			    WHEN FieldName = 'UnitCondition'		THEN 'Unit Condition'
			    WHEN FieldName = 'ServiceProviderID'	THEN 'Service Provider'
			    WHEN FieldName = 'DateBooked'		THEN 'Date Booked'
			    WHEN FieldName = 'OpenJobStatus'		THEN 'Branch Open Job Status'
			    

			    ELSE FieldName
			END							    AS '3',

			CASE
			    WHEN FieldName = 'ProductID' THEN (SELECT p.ProductNo FROM product AS p WHERE p.ProductID = audit_new.OldValue)
			    WHEN FieldName = 'ManufacturerID' THEN (SELECT m.ManufacturerName FROM manufacturer AS m WHERE m.ManufacturerID = audit_new.OldValue)
			    WHEN FieldName = 'ServiceTypeID' THEN (SELECT s.ServiceTypeName FROM service_type AS s WHERE s.ServiceTypeID = audit_new.OldValue)
			    WHEN FieldName = 'ServiceProviderID' THEN (SELECT CompanyName FROM service_provider AS sp WHERE sp.ServiceProviderID = audit_new.OldValue)
			    WHEN FieldName = 'OpenJobStatus' THEN   CASE
									WHEN audit_new.OldValue = 'awaiting_parts' THEN 'Awaiting Parts'
                                                                        WHEN audit_new.OldValue = 'in_store' THEN 'Awaiting Service Provider Collection'
									WHEN audit_new.OldValue = 'with_supplier' THEN 'Job In Progress With Service Provider'
									WHEN audit_new.OldValue = 'awaiting_collection' THEN 'Service Complete Returned to Branch'
									WHEN audit_new.OldValue = 'customer_notified' THEN 'Customer Notified'
									WHEN audit_new.OldValue = 'closed' THEN 'Returned to customer'
								    END
			    ELSE OldValue
			END							    AS '4',

			CASE
			    WHEN FieldName = 'ProductID' THEN (SELECT p.ProductNo FROM product AS p WHERE p.ProductID = audit_new.NewValue)
			    WHEN FieldName = 'ManufacturerID' THEN (SELECT m.ManufacturerName FROM manufacturer AS m WHERE m.ManufacturerID = audit_new.NewValue)
			    WHEN FieldName = 'ServiceTypeID' THEN (SELECT s.ServiceTypeName FROM service_type AS s WHERE s.ServiceTypeID = audit_new.NewValue)
			    WHEN FieldName = 'ServiceProviderID' THEN (SELECT CompanyName FROM service_provider AS sp WHERE sp.ServiceProviderID = audit_new.NewValue)
			    WHEN FieldName = 'OpenJobStatus' THEN   CASE									
                                                                        WHEN audit_new.NewValue = 'awaiting_parts' THEN 'Awaiting Parts'
                                                                        WHEN audit_new.NewValue = 'in_store' THEN 'Awaiting Service Provider Collection'
									WHEN audit_new.NewValue = 'with_supplier' THEN 'Job In Progress With Service Provider'
									WHEN audit_new.NewValue = 'awaiting_collection' THEN 'Service Complete Returned to Branch'
									WHEN audit_new.NewValue = 'customer_notified' THEN 'Customer Notified'
									WHEN audit_new.NewValue = 'closed' THEN 'Returned to customer'
								    END
			    ELSE NewValue
			END							    AS '5'

		FROM	audit_new 
		WHERE	TableName = 'job' AND 
			PrimaryID = :jobID AND
			FieldName != 'StatusID'
                        
                        ORDER BY `Date` DESC
	     ";
		
	$values = ["jobID" => $jobID];
	$result = $this->query($this->conn, $q, $values);
        
//        $this->log("ddsdfsdfsdfs2222222222222");
//        $this->log($result);
        
	return (count($result) > 0) ? $result : false;
	
    }
    
    
    
}
?>
