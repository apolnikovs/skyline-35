<?php
require_once('CustomRESTClient.class.php');
require_once('Functions.class.php');
require_once('Constants.class.php');

/**
 * Short Description of ServiceBase API Client.
 * 
 * Long description of ServiceBase API Client.
 *
 * @author     Brian Etherington <b.etherington@pccsuk.com>
 * @copyright  2012 PC Control Systems
 * @link       http://www.pccontrolsystems.com
 * @version    1.0
 * 
 *  
 * Changes
 * Date        Version Author                Reason
 * 09/07/2013 1.0      Brian Etherington     Initial Version
 * **************************************************************************** */
class ServiceBaseAPIClient extends CustomRESTClient{
    
    public function __construct($controller) {
        parent::__construct($controller);
    }

    public function PutJobDetails($url, $params) {        
        return $this->execute($url.'/PutJobDetails', $params, 'POST');
    }
}
