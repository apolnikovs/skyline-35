<?php
require_once('CustomModel.class.php');
require_once('TableFactory.class.php');

/**
 * ContactHistory.class.php
 * 
 * This model handles interacttion with the contact_history table.
 * 
 * @author      Andrew J. Williams <a.williams@pccsuk.com>
 * @version     1.16
 * @copyright   2012 - 2013 PC Control Systems Ltd
 * 
 * Changes
 * Date        Version Author               Reason
 * 22/06/2012  1.00    Andrew J. Williams   Initial Version
 * 06/07/2012  1.01    Andrew J. Williams   Responses changed to match jobs model
 * 20/11/2012  1.02    Brian Etherington    Added Sort order to fetchForJob method
 *                                          use DISTINCT to remove duplicates
 *                                          change minutes FORMAT specifier to %i
 * 14/01/2013  1.03    Andrew J. Williams    Issue 177 - Skyline export changes
 * 01/03/2013  1.04    Andrew J. Williams   Issue 241 - Duplicate Contact History Records
 * 27/03/2013  1.05    Vykintas Rutkunas    Job Update Fix
 * 19/04/2013  1.06    Andrew J. Williams   Data Integrity Check - Refresh Process
 * 19/04/2013  1.07    Andrew J. Williams   Fix table name in delete method
 * 29/04/2013  1.08    Andrew J. Williams   Issue 330 - Some Contact History Items not being Updated
 * 29/04/2013  1.09    Brian Etherington    Change date format in getContactHistory
 * 03/05/2013  1.10    Andrew J. Williams   Issue 349 - Urgent changes for Skyline to RMA
 * 13/05/2013  1.11    Vykintas Rutkunas    Job Update page changes
 * 18/05/2013  1.12    Andrew J. Williams   Updates to Contact History to takwe into account Source Field (Contact History Vs Remote Engineer)
 * 15/05/2013  1.13    Vykintas Rutkunas    Contact history fix	
 * 16/05/2013  1.14    Andrew J. Williams   Issue 363 - Errors in log file for API
 * 03/06/2013  1.15    Brian Etherington    Fix typo in getNumberForJob sql syntax
 * 19/06/2013  1.16    Brian Etherington    updated checkExists to use PDO parameters
 *                                          mysql_real_escape_string deprecated
 ******************************************************************************/

class ContactHistory extends CustomModel { 
    
    private $table;                                                             /* For Table Factory Class */
    private $conn;                                                              /* Datbase connection */
    
    public function __construct($Controller) {
                  
        parent::__construct($Controller);
        
        $this->conn = $this->Connect( $this->controller->config['DataBase']['Conn'],
                                      $this->controller->config['DataBase']['Username'],
                                      $this->controller->config['DataBase']['Password'] );  
        
        $this->table = TableFactory::ContactHistory();
    }
       
    /**
     * create
     *  
     * Create a Contact History item
     * 
     * @param array $args   Associative array of field values for the creation of
     *                      the new appointment
     * 
     * @return array    (status - Status Code, message - Status message, id - Id of inserted item
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    public function create($args) {
        $cmd = $this->table->insertCommand( $args );
        if ( $this->Execute($this->conn, $cmd, $args) ) {            
            $result = array (
                             'status' => 'SUCCESS',
                             'contactHistoryId' => $this->conn->lastInsertId()
                            );
        } else {
            $result = array(
                            'status' => 'FAIL',
                            'contactHistoryId' => 0,
                            'message' => $this->lastPDOError()
                           );
        }
        
        return ( $result );
    }
    
    /**
     * update
     *  
     * Update a ContactHistoryItem
     * 
     * @param array $args   Associative array of field values for to update the
     *                      appointment. The array must include the primary key
     *                      Contact History Item 
     * 
     * @return (status - Status Code, message - Status message, rows_affected number of rows updated)
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    public function update($args) {
        $cmd = $this->table->updateCommand( $args );
        
        if ($this->Execute($this->conn, $cmd, $args)) {
            $result = array(
                            'status' => 'SUCCESS',
                            'message' => ''
                           );
        } else {
            $result = array(
                            'status' => 'FAIL',
                            'message' => $this->lastPDOError()
                           );          
        }
        return $result;
    }
    
    /**
     * delete
     *  
     * Delete a Call History item. As Service base does not know ContactHistoryID
     * we need to delete on date, time and JobID as this is unique.
     * 
     * @param Date $chD     Date of contact history item
     * @param Time $chT     Time of Contact History Item
     * @param integer $jId  Job ID
     * 
     * @return (status - Status Code, message - Status message, rows_affected number of rows deleted)
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    public function delete($chD, $chT, $jId) {      
        $cmd = $this->table->deleteCommand("
                                            `ContactDate` = '$chD' 
                                            AND `ContactTime` = '$chT'
                                            AND `JobID` = $jId
                                           "
                                          );
        
        $rows_affected = $this->Execute($this->conn, $cmd);
        
        if ( $rows_affected == 0 ) {                                             /* No rows affected may be error */
            $result = array(
                            'status' => 'FAIL',
                            'message' => 'No rows deleted'
                           ); 
        } else {
            $result = array(
                            'status' => 'SUCCESS',
                            'message' => 'Deleted'
                           ); 
        }
        
        return ( $result );
    }
    
    /**
     * deleteByJobId
     *  
     * Delete all the contact history for a specific job.
     * 
     * This is specifcally used in Data Integrity Check Refresh Process
     * 
     * @param array $jId    JobID
     * 
     * @return true if sucessful, false otherwise
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    public function deleteByJobId($jId) {
        $sql = "
                DELETE
		FROM
			`contact_history`
		WHERE
			`JobID` = $jId
               ";
        
        $count = $this->conn->exec($sql);

        if ( $count !== false ) {
            return(true);                         
        } else {
            return(false);                                 
        }
    }
    
    /**
     * CheckExists
     *  
     * Check whether a Contact His
     * 
     * @param Date $chD      Date of contact history item
     * @param Time $chT      Time of Contact History Item
     * @param integer $jId   Job ID
     * @param integer $chNote Contact History Note
     * 
     * @return Integer containg whether the items already exists  
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    public function CheckExists($chD, $chT, $jId, $chNote) {      
        /*$sql = "
                SELECT
			COUNT(`ContactHistoryID`) AS `count`
		FROM
		        `contact_history`
		WHERE
                        `ContactDate` = '$chD' 
                        AND `ContactTime` = '$chT'
                        AND `JobID` = $jId
                        AND `Note` = '".mysql_real_escape_string($chNote)."'
               "; */
        
        $sql = "
                SELECT
			COUNT(`ContactHistoryID`) AS `count`
		FROM
		        `contact_history`
		WHERE
                        `ContactDate` = :ContactDate 
                        AND `ContactTime` = :ContactTime
                        AND `JobID` = :JobID
                        AND `Note` = :Note";
        
        $params = array( 'ContactDate' => $chD,
                         'ContactTime' => $chT,
                         'JobID' => $jId,
                         'Note' => $chNote);

        $result = $this->Query($this->conn, $sql, $params);
        
        return($result[0]['count']);
    }
    
    
    
    public function fetchForJob($jID) {

	/*
        $args['where'] = "JobID = $jID";
	$columns = array(   'ContactDate', 
			    'TIME_FORMAT(ContactTime,"%H:%i") AS ContactTime', 
			    'UserCode', 
			    'ContactHistoryActionID', 
			    'Subject', 
			    'Note'
			);
        //$output = $this->ServeDataTables($this->conn, 'contact_history', array('ContactDate', 'ContactTime', 'UserCode', 'ContactHistoryActionID', 'Subject', 'Note'), $args);
        $output = $this->ServeDataTables($this->conn, 'contact_history', $columns, $args);

	//$this->controller->log("outpt: " . var_export($output,true));
	*/
	
	$q = "	SELECT DISTINCT	DATE_FORMAT(t1.ContactDate, '%Y-%m-%d')	AS '0', 
				TIME_FORMAT(t1.ContactTime,'%H:%i')	AS '1', 
				CASE
				    WHEN t1.UserCode IS NOT NULL AND t1.UserCode != 0
				    THEN SELECT CONCAT(user.ContactFirstName, ' ', user.ContactLastName) FROM user WHERE user.UserID = t1.UserCode
				    ELSE 'N/A'
				END					AS '2',
				t2.Action				AS '3',
				t1.Subject				AS '4',
				t1.Note					AS '5'
				
		FROM		contact_history AS t1
		
		LEFT JOIN	contact_history_action AS t2 ON t1.ContactHistoryActionID=t2.ContactHistoryActionID
		
		WHERE		t1.JobID=:jobID
		
                ORDER BY	t1.ContactDate,t1.ContactTime
	    ";
	
	$values = array("jobID" => $jID);
	$output = $this->query($this->conn,$q,$values);
	
	//$this->controller->log("outpt: " . var_export(array("aaData" => $output),true));
	
	return  (array("aaData" => $output));
	
    }
    
    
    
    public function getContactHistory($args, $JobID) {
      
        $args['where'] = "JobID = $JobID AND t2.Source != 'Remote Engineer'";
        $args['DISTINCT'] = true; // KLUGE alert!!!! this is to remove invalid duplicate rows in the select.
                                  // In case you are wondering, there is no attempt to resolve the cause of 
                                  // the duplicate rows in the RMA Tracker API.
                                  // Duplicates jobs fixed as of Issue 241
                                  // https://bitbucket.org/pccs/skyline/issue/241/duplicate-contact-history-records
        
        $tables = ' contact_history AS t1 
		    LEFT JOIN contact_history_action AS t2 ON t1.ContactHistoryActionID = t2.ContactHistoryActionID ';
//changed display date format by thirumal..%d/%m/%y replace with %d/%/m%Y
	$columns = [
	    ["t1.ContactDate", "DATE_FORMAT(t1.ContactDate, '%d/%m/%Y')"], 
	    ["t1.ContactTime", "TIME_FORMAT(t1.ContactTime, '%H:%i')"],
	    "t1.UserCode",
	    "t2.Action",
	    "t1.Subject",
	    "t1.Note",
	    /*"	CASE
		    WHEN t1.Private = 'Y'
		    THEN '<span class=\"hoverCell\" title=\"Private\" style=\"width:100%; display:block; text-align:center;\"><img src=\"" . SUB_DOMAIN . "/images/no-entry.png\" /></span>'    
		    ELSE NULL
		END
	    "*/
	    "NULL"
	];
	
        $contact_history = $this->ServeDataTables($this->conn, $tables, $columns, $args);

        return $contact_history;
        
    }

    
    
    /**
     * getNumberForJob
     *  
     * Get the number of contact history entries for a specfifc job
     * 
     * @param jId   ID of job to be searched for 
     * 
     * @return      Count of contact history rows for job
     *
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    
    public function getNumberForJob($JobID) {
        
	$sql = "SELECT	    COUNT(*) AS count 
		FROM	    contact_history 
		WHERE	    JobID = :JobID
	      ";
        $params = array( 'JobID' => $JobID );
        
        $result = $this->Query($this->conn, $sql, $params);
        
        return($result[0]["count"]);
	
    }

    
    
    /**
     * GetImage
     *  
     * Get the iimage uploaded by the remote engineer for a specific contact 
     * history item.
     * 
     * @param integer $chId    Conatct History ID
     * 
     * @return Array of data to be used by the controller to diaply uploaded
     *         image.
     * 
     * @author Andrew Williams <Andrew.Williams@awcomputech.com>  
     **************************************************************************/
    public function GetImage($chId) {      
        $sql = "
                SELECT
			CONCAT(sp.`IPAddress`,':', sp.`Port`, ch.`ImageURL`) AS ImageURL,
			ch.`Note`
		FROM
		        `contact_history` ch LEFT JOIN `job` j ON ch.`JobID` = j.`JobID` 
			                     LEFT JOIN `service_provider` sp ON j.`ServiceProviderID` = sp.`ServiceProviderID`
		WHERE
                        `ContactHistoryID` = :ContactHistoryID
               ";

        $params = array( 'ContactHistoryID' => $chId );
        
        $result = $this->Query($this->conn, $sql, $params);
        
        return($result[0]);
    }
    
}
?>
