<?php

require_once('CustomModel.class.php');

/**
 * Description
 *
 * This class is used for handling database actions of Client Service Types Page in Product Setup section under System Admin
 *
 * @author      Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
 * @version     1.2
 * 
 * 03/09/2012   1.1    Nageswara Rao       Kanteti Fetch query bug is fixed 
 * 02/10/2012   1.2    Brian Etherington   Changed user->DefaultBrand to user->DefaultBrandID
 */

class ClientServiceTypes extends CustomModel {
    
    private $conn;
    
    private $table                      = "service_type";
    private $table_service_type_alias   = "service_type_alias";
    private $tables                     = "service_type AS T1 LEFT JOIN service_type_alias AS T2 ON T1.ServiceTypeID=T2.ServiceTypeID LEFT JOIN job_type AS t3 ON t3.JobTypeID=T1.JobTypeID AND T1.BrandID=t3.BrandID";
    private $dbTableColumns             = array('T1.ServiceTypeID', 't3.Type', 'T1.ServiceTypeName', 'T1.Status');
    private $dbTableColumns2            = array('T1.ServiceTypeID', 't3.Type', 'T1.ServiceTypeName', 'T1.Status', array('T1.ServiceTypeID', 'T1.ServiceTypeID AS Mask'), array('T1.ServiceTypeID', 'T1.ServiceTypeID AS Assigned'));
   
   
      
    public function __construct($controller) {
    
        parent::__construct($controller); 

        $this->conn = $this->Connect( $this->controller->config['DataBase']['Conn'],
                                      $this->controller->config['DataBase']['Username'],
                                      $this->controller->config['DataBase']['Password'] );       

    }
    
   
    
     /**
     * Description
     * 
     * This method is for fetching data from database
     * 
     * @param array $args Its an associative array contains where clause, limit and order etc.
     * @global $this->conn
     * @global $this->table 
     * @global $this->tables
     * 
     * @global $this->dbTableColumns
     * @global $this->dbTableColumns2 
     * @return array 
     * 
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */  
    
    public function fetch($args) {
        
        
        $NetworkID     = isset($args['firstArg'])?$args['firstArg']:'';
        $ClientID      = isset($args['secondArg'])?$args['secondArg']:'';
        $showAssigned  = isset($args['thirdArg'])?$args['thirdArg']:'';
        
        $tables2         = $this->table." AS T1 LEFT JOIN job_type AS t3 ON t3.JobTypeID=T1.JobTypeID AND T1.BrandID=t3.BrandID";; 
        
        if($NetworkID!='' && $ClientID!='')
        {
            if($showAssigned)
            {
                $args['where']    = "T1.Status='".$this->controller->statuses[0]['Code']."' AND T1.BrandID='".$this->controller->user->DefaultBrandID."' AND T2.NetworkID='".$NetworkID."' AND T2.ClientID='".$ClientID."' AND T2.Status='".$this->controller->statuses[0]['Code']."'";
                
                $output = $this->ServeDataTables($this->conn, $this->tables, $this->dbTableColumns2, $args);
            }   
            else
            {    
                $args['where']    = "T1.Status='".$this->controller->statuses[0]['Code']."' AND T1.BrandID='".$this->controller->user->DefaultBrandID."' GROUP BY T1.ServiceTypeID";
                $output = $this->ServeDataTables($this->conn, $this->tables, $this->dbTableColumns2, $args);
            }
        }
        else if($NetworkID!='')
        {
                $args['where']    = "T1.Status='".$this->controller->statuses[0]['Code']."' AND T1.BrandID='".$this->controller->user->DefaultBrandID."'";
                $output = $this->ServeDataTables($this->conn, $tables2, $this->dbTableColumns, $args);
        }
        else
        {
             $args['where']    = "T1.ServiceTypeID='0'";
             $output = $this->ServeDataTables($this->conn, $tables2, $this->dbTableColumns, $args);
        }
       
        
        return  $output;
        
    }
    
    
    
    /**
     * Description
     * 
     * This method calls update method if the $args contains primary key.
     * 
     * @param array $args Its an associative array contains all elements of submitted form.
    
     * @return array It contains status and message.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com> 
     */   
     public function processData($args) {
         
         if(!isset($args['ServiceTypeAliasID']) || !$args['ServiceTypeAliasID'])
         {
               return $this->create($args);
         }
         else
         {
             return $this->update($args);
         }
     }
    
     
    
    
    
     /**
     * Description
     * 
     * This method is used for to check whether client is exists for the Service Type.
     
     * @param interger $NetworkID 
     * @param interger $ClientID 
     * @param interger $ServiceTypeID
     * @param boolean  $returnArray
     *  
     * @global $this->table_service_type_alias
     
     * 
     * @return boolean.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
     public function isClientExists($NetworkID, $ClientID, $ServiceTypeID, $returnArray=false) {
        
         /* Execute a prepared statement by passing an array of values */
        $sql = 'SELECT *  FROM '.$this->table_service_type_alias.' a 
        join service_type st on st.ServiceTypeID=a.ServiceTypeID    
        WHERE a.ServiceTypeID=:ServiceTypeID AND a.ClientID=:ClientID AND a.NetworkID=:NetworkID';
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $fetchQuery->execute(array(':ServiceTypeID' => $ServiceTypeID, ':NetworkID' => $NetworkID, ':ClientID' => $ClientID));
        $result = $fetchQuery->fetch();
       
        if($returnArray)
        {
            return $result;
        }    
        else if(is_array($result) && $result['ServiceTypeAliasID'])
        {
                return true;
        }
        
        return false;
    
    }
    
    
    /**
     * Description
     * 
     * This method is used for to udpate a row into database.
     *
     * @param array $args
     * @global $this->table_service_type_alias   
     * @return array It contains status of operation and message.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
    public function update($args) {
        
        if($args['ServiceTypeAliasID'])
        {
            if($args['Status']=='Active')
            {
               
                $EndDate = "0000-00-00 00:00:00";
                
            } 
            else
            {
              
                $EndDate = date("Y-m-d H:i:s");
                
            }
           
            
            //andris change 21/6/2013 Joes Request to be able set warranty and chargable defaults
            isset($args['Warranty'])?$Warranty="Y":$Warranty="N";
            isset($args['Chargeable'])?$Chargeable="Y":$Chargeable="N";
            $sql="update service_type set Warranty=:Warranty , Chargeable=:Chargeable where ServiceTypeID=:ServiceTypeID";
            $param=[
                'ServiceTypeID'=>$args['ServiceTypeID'],
                'Warranty'=>$Warranty,
                'Chargeable'=>$Chargeable
            ];
          
            $this->Execute($this->conn, $sql,$param);
            //end andris change
            
            
            /* Execute a prepared statement by passing an array of values */
            $sql = 'UPDATE '.$this->table_service_type_alias.' SET ServiceTypeMask=:ServiceTypeMask, EndDate=:EndDate, Status=:Status, ModifiedUserID=:ModifiedUserID, ModifiedDate=:ModifiedDate
            WHERE ServiceTypeAliasID=:ServiceTypeAliasID AND ServiceTypeID=:ServiceTypeID AND NetworkID=:NetworkID AND ClientID=:ClientID';
        
              $updateQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
              $updateQuery->execute(array(
                  
                    ':ServiceTypeAliasID' => $args['ServiceTypeAliasID'], 
                    ':ServiceTypeMask' => ($args['ServiceTypeMask'])?$args['ServiceTypeMask']:NULL, 
                    ':ServiceTypeID' => $args['ServiceTypeID'],
                    ':Status' => $args['Status'], 
                    ':NetworkID' => $args['NetworkID'], 
                    ':ClientID' => $args['ClientID'],
                    ':EndDate' => $EndDate,
                    ':ModifiedUserID' => $this->controller->user->UserID,
                    ':ModifiedDate' => date("Y-m-d H:i:s")
                  
                  ));
              
             
                
               return array('status' => 'OK',
                        'message' => 'Your data has been updated successfully.');
        }
        else
        {
             return array('status' => 'ERROR',
                        'message' => $this->controller->messages->getError(1024, 'default', $this->controller->lang));
        }
    }
    
    
    
    /**
     * Description
     * 
     * This method is used for to insert data into database.
     *
     * @param array $args  
     * @global $this->table_service_type_alias 
     * @return array It contains status of operation and message.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
    public function create($args) {
        
       
            
            if($args['ServiceTypeID'] && $args['NetworkID'] && $args['ClientID'])
            {

                    if($args['Status']=='Active')
                    {

                        $EndDate = "0000-00-00 00:00:00";

                    } 
                    else
                    {

                        $EndDate = date("Y-m-d H:i:s");

                    }

                    //Inserting details into $this->table_service_type_alias
                    /* Execute a prepared statement by passing an array of values */
                    $sql3 = 'INSERT INTO '.$this->table_service_type_alias.' (ServiceTypeMask, NetworkID, ClientID, ServiceTypeID, EndDate, CreatedDate, Status, ModifiedUserID, ModifiedDate)
                    VALUES(:ServiceTypeMask, :NetworkID, :ClientID, :ServiceTypeID, :EndDate, :CreatedDate, :Status, :ModifiedUserID, :ModifiedDate)';

                    $insertQuery3 = $this->conn->prepare($sql3, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));


                     $result = $insertQuery3->execute(array(


                                                ':ServiceTypeMask' => ($args['ServiceTypeMask'])?$args['ServiceTypeMask']:NULL, 
                                                ':ServiceTypeID' => $args['ServiceTypeID'],
                                                ':Status' => $args['Status'], 
                                                ':NetworkID' => $args['NetworkID'], 
                                                ':ClientID' => $args['ClientID'],
                                                ':EndDate' => $EndDate,
                                                ':CreatedDate' => date("Y-m-d H:i:s"),
                                                ':ModifiedUserID' => $this->controller->user->UserID,
                                                ':ModifiedDate' => date("Y-m-d H:i:s")


                                                ));


                      return array('status' => 'OK',
                                'message' => 'Your data has been inserted successfully.');
        }
        else
        {
            
            return array('status' => 'ERROR',
                        'message' => $this->controller->messages->getError(1024, 'default', $this->controller->lang));
        }
    }
    
    
   
    
     
     /**
     * Description
     * 
     * This method is used for to assign client to selected Service Types.
     *
     * @param int    $NetworkID 
     * @param int    $ClientID
     * @param array  $assignedClientServiceTypes
     * @param string $sltClientServiceTypes    
     
     * @global $this->table_service_type_alias
     *    
     * @return array It contains status of operation and message.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
    public function updateClientServiceTypes($NetworkID, $ClientID, $assignedClientServiceTypes, $sltClientServiceTypes){
        
             $result = false;
        
            
            //Updating details into $this->table_service_type_alias
            $sql2 = 'UPDATE '.$this->table_service_type_alias.' SET EndDate=:EndDate, Status=:Status, ModifiedUserID=:ModifiedUserID, ModifiedDate=:ModifiedDate WHERE NetworkID=:NetworkID AND ClientID=:ClientID AND ServiceTypeID=:ServiceTypeID';

            /* Execute a prepared statement by passing an array of values */    
            $updateQuery2 = $this->conn->prepare($sql2, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));


            //Inserting details into $this->table_service_type_alias
            /* Execute a prepared statement by passing an array of values */
            $sql3 = 'INSERT INTO '.$this->table_service_type_alias.' (NetworkID, ClientID, ServiceTypeID, EndDate, CreatedDate, Status, ModifiedUserID, ModifiedDate)
            VALUES(:NetworkID, :ClientID, :ServiceTypeID, :EndDate, :CreatedDate, :Status, :ModifiedUserID, :ModifiedDate)';

            $insertQuery3 = $this->conn->prepare($sql3, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));


           
            $sltClientServiceTypesList = explode(",", $sltClientServiceTypes);
            
           
            if(is_array($sltClientServiceTypesList))
            {
                    if(!is_array($assignedClientServiceTypes))
                    {   
                        $assignedClientServiceTypes = array();
                    }
                
                    foreach ( $sltClientServiceTypesList as $autKey=>$autValue)
                    {

                        if($autValue)
                        {    
                                $existsResult = $this->isClientExists($NetworkID, $ClientID, $autValue);



                                $EndDate = date("Y-m-d H:i:s");
                                $args['Status'] = $this->controller->statuses[1]['Code'];

                                //If the assigned check box is checked then we are assigning active as status and end date to null.
                                 
                                if(in_array($autValue, $assignedClientServiceTypes))
                                {
                                    $EndDate = "0000-00-00 00:00:00";
                                    $args['Status'] = $this->controller->statuses[0]['Code'];
                                }
                                        
                                $result = true;
                                if($existsResult)
                                {
                                    $result = $updateQuery2->execute(array(
                                    
                                    ':NetworkID' => $NetworkID,     
                                    ':ClientID' => $ClientID, 
                                    ':ServiceTypeID' => $autValue,    
                                    ':EndDate' => $EndDate,
                                    ':Status' => $args['Status'],
                                    ':ModifiedUserID' => $this->controller->user->UserID,
                                    ':ModifiedDate' => date("Y-m-d H:i:s")

                                    ));
                                }
                                else if($args['Status']!=$this->controller->statuses[1]['Code'])//If the relation is not exists in database and assigned checkbox is ticked then we are inserting relation into database.
                                {

                                        $result = $insertQuery3->execute(array(

                                        ':NetworkID' => $NetworkID,    
                                        ':ClientID' => $ClientID, 
                                        ':ServiceTypeID' => $autValue,    
                                        ':EndDate' => $EndDate,    
                                        ':CreatedDate' => date("Y-m-d H:i:s"),
                                        ':Status' => $args['Status'],
                                        ':ModifiedUserID' => $this->controller->user->UserID,
                                        ':ModifiedDate' => date("Y-m-d H:i:s")

                                        ));

                                }
                            
                        }   

                    }
            
            }
        
        return $result;
    
    }
     
     
    
    
}
?>
