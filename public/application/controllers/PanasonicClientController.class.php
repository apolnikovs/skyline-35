<?php

/**
 * SamsungClientController.class.php
 * 
 * Implementation of Client for Panasonic API
 *
 * @author     Andrew Williams <a.williams@pccsuk.com>
 * @copyright  2012 PC Control Systems
 * @link       
 * @version    1.0
 * 
 * Changes
 * Date        Version Author                Reason
 * 29/01/2013  1.00    Andrew J. Williams    Initial Version
 ******************************************************************************/

class PanasonicClientController extends CustomSmartyController  {
    public $debug = false;                                                      /* Turn on or off debugging */
    public $config;  
    public $session;
    public $messages;
     
    public function __construct() {
        parent::__construct(); 

        $this->config = $this->readConfig('application.ini');                   /* Read Application Config file. */
        $this->session = $this->loadModel('Session');                           /* Initialise Session Model */
        $this->messages = $this->loadModel('Messages');                         /* Initialise Messages Model */
    }
    
    /**
     * TLR
     * 
     * Perform tlr request (get new tickets to Panasonic)
     * 
     * @param $args
     * 
     * @return 
     * 
     * @author Andrew Williams <a.williams@pccsuk.com> 
     **************************************************************************/
    public function TLRAction($args) {
        $client = new SoapClient(APPLICATION_PATH.'/wsdl/'.$this->config['Panasonic']['PanasonicApiWsdl']);
        
	$xml = '
            <?xml version="1.0" encoding="utf-8"?>
            <DSDF xmlns="http://www.digitaleurope.org/ns/dsdf">
              <mainData>
                <mainDataBatchDocumentType>TLR</mainDataBatchDocumentType>
                <mainDataVersionNumber>12</mainDataVersionNumber>
                <mainDataManufacturerName>'.$this->config['Panasonic']['ManufacturerName'].'</mainDataManufacturerName>
                <mainDataManufacturerCountryID>GB</mainDataManufacturerCountryID>
                <mainDataSystemUserAuthentication>MD5P</mainDataSystemUserAuthentication>
                <mainDataSystemUserLogin>'.$this->config['Panasonic']['PanasonicUser'].'</mainDataSystemUserLogin>
                <mainDataSystemUserPassword>'.$this->config['Panasonic']['PanasonicPass'].'</mainDataSystemUserPassword>
                <mainDataSystemUserLanguage>GB</mainDataSystemUserLanguage>
                <mainDataCustomerIDSystem>'.$this->config['Panasonic']['PanasonicCustomerIDSystem'].'</mainDataCustomerIDSystem>
              </mainData>
            </DSDF>
        ';
        
	$response = $client->DSDF($xml); 
        
        if ($this->debug) $this->log('TLR : xml Response:\n'.var_export($response,true),'panasonic_api_');
        
        if ( $response['DSDF']['mainData']['mainDataSystemResultCode'] != 0) {  /* Error in response */
            $this->log('PansonicClientConroller : TLR : Error in reponse','panasonic_api_');
            $this->log('PansonicClientConroller : TLR : xml Request:\n'.var_export($xml,true),'panasonic_api_');
            $this->log('PansonicClientConroller : TLR : xml Response:\n'.var_export($response,true),'panasonic_api_');
            return;
        }
        
        foreach($response['DSDF']['mainDataGroup']['MainDataItem'] as $pJob) {
            if (isset($pJob['mainDataIncidentNumberSystem'])) {                 /* Check we have a Panasonic job number */
                $result[$pJob['mainDataIncidentNumberSystem']] = $this->TRQAction(array('pJob' => $pJob['mainDataIncidentNumberSystem']));  /* Yes so process job details */
            } else {
                $this->log('No job details found','panasonic_api_','panasonic_api_');                               /* No job (mainDataIncidentNumberSystem) number found */
                $this->log(var_export($pJob,true),'panasonic_api_');
            }
        }
        return($result);
    }

    
    /**
     * TLR
     * 
     * Perform tlr request (get new tickets to Panasonic)
     * 
     * @param $args
     * 
     * @return 
     * 
     * @author Andrew Williams <a.williams@pccsuk.com> 
     **************************************************************************/
    
    public function TRQAction($args) {
        $manufacturers_model = $this->loadModel('Manufacturers');
        $models_model = $this->loadModel('Models');
        $unit_types_model = $this->loadModel('UnitTypes');
        
	$xml = '
            <?xml version="1.0" encoding="utf-8"?>
            <DSDF xmlns="http://www.digitaleurope.org/ns/dsdf">
              <mainData>
                <mainDataBatchDocumentType>TRQ</mainDataBatchDocumentType>
                <mainDataVersionNumber>12</mainDataVersionNumber>
                <mainDataManufacturerName>'.$this->config['Panasonic']['ManufacturerName'].'</mainDataManufacturerName>
                <mainDataManufacturerCountryID>GB</mainDataManufacturerCountryID>
                <mainDataSystemUserAuthentication>MD5P</mainDataSystemUserAuthentication>
                <mainDataSystemUserLogin>'.$this->config['Panasonic']['PanasonicUser'].'</mainDataSystemUserLogin>
                <mainDataSystemUserPassword>'.$this->config['Panasonic']['PanasonicPass'].'</mainDataSystemUserPassword>
                <mainDataSystemUserLanguage>GB</mainDataSystemUserLanguage>
                <mainDataIncidentNumberSystem>'.$args['pJob'].'</mainDataIncidentNumberSystem>
              </mainData>
            </DSDF>
        ';

        $response = $client->DSDF($xml); 
        
        if ($this->debug) $this->log('TLR : xml Response:\n'.var_export($response,true),'panasonic_api_');
        
        if ( $response['DSDF']['mainData']['mainDataSystemResultCode'] != 0) {  /* Error in response */
            $this->log('PansonicClientConroller : TLR : Error in reponse','panasonic_api_');
            $this->log('PansonicClientConroller : TLR : xml Request:\n'.var_export($xml,true),'panasonic_api_');
            $this->log('PansonicClientConroller : TLR : xml Response:\n'.var_export($response,true),'panasonic_api_');
            return;
        }

        $data = $response['DSDF'];                                              /* All data is in the DSDF coontainer */
        
        /*
         * Manufacturer
         */
        if (isset($data['mainData']['mainDataManufacturerName'])) {             /* Deal with manufacturer field if set */  
            $job_fields['ManufacturerID'] = $manufacturers_model->getManufacturerId($data['Manufacturer']); /* Get the ID of the manufacturer */
            if (is_null($job_fields['ManufacturerID'])) {                       /* Manufacturer not found */
                unset ($job_fields['ManufacturerID']);                          /* No Match, so don't want ManufacturerID */
                $job_fields['ServiceBaseManufacturer'] = $data['mainData']['mainDataManufacturerName'];   /* So set the free text ServiceBaseManufacturer */
            } /* is_null $fields['ManufacturerID'] */
        }
        
        if (isset($data['mainData']['mainDataIncidentNumberCall'])) $job_fields['AgentRefNo'] = $data['mainData']['mainDataIncidentNumberCall'];
        if (isset($data['mainData']['mainDataIncidentNumberManufacturer'])) $job_fields['NetworkRefNo'] = $data['mainData']['mainDataIncidentNumberManufacturer'];
        
        /*
         * Unit Tyoe
         */
        if (isset($data['productData']['productDataBrandName'])) {              /* Deal with unit type field if set */  
            $job_fields['UnitTypeID'] = $unit_types_model->getId($data['productData']['productDataBrandName']);     /* Get the ID of the unit type */
            if (is_null($job_fields['UnitTypeID'])) {                                                               /* Unit type ID not found so don't write */
                unset ($job_fields['UnitTypeID']);                              /* No Match - Don't want to set ID field in database as it is a foreign key */
                $job_fields['ServiceBaseUnitType'] = $data['productData']['productDataBrandName'];  /* and set ServiceBaseUnitType free text unit type  */ 
            }
        }
        
        /*
         * Model
         */

        /* If we have a model number and a unit type ID then get Model ID (If we have no UnitTypeID we don't want a model ID as we need both, See VMS Trackerbase Log 140 */
        if ( isset($data['productData']['productDataProductTypeNumber']) && isset($job_fields['UnitTypeID']) ) {
            $job_fields['ModelID'] = $models_model->getModelIdFromNo($data['productData']['productDataProductTypeNumber']);        /* Get the ID of the model */
            if (is_null($fields['ModelID'])) {                                                                      /* Model ID not found */
                unset($job_fields['ModelID']);
                $job_fields['ServiceBaseUnitType'] = $data['productData']['productDataProductTypeNumber'];
            } /* fi is_null $fields['ModelID'] */
        }

        if ( isset($data['productData']['productDataSerialNumber']) ) $job_fields['SerialNo'] = $data['productData']['productDataSerialNumber'];
        if ( isset($data['productData']['productDataPurchaseDate']) ) $job_fields['DateOfPurchase'] = $data['productData']['productDataPurchaseDate'];
        if ( isset($data['productData']['productDataExtendedGuaranteeNumber']) ) $job_fields['ExtendedWarrantyNo'] = $data['productData']['productDataExtendedGuaranteeNumber'];
        /* $data['productData']['productDataStockware'] to be mapped to Service Type */
        if ( isset($data['productData']['productDataSource']) ) $job_fields['OriginalRetailer'] = $data['productData']['productDataSource'];
        if ( isset($data['productData']['productDataAccessoriesExtended']) ) $job_fields['Accessories'] = $data['productData']['productDataAccessoriesExtended'];
        if ( isset($data['productData']['productDataCaseConditionExtended']) ) $job_fields['UnitCondition'] = $data['productData']['productDataCaseConditionExtended'];
        
        /* $data['repairData']['repairDataCreatorRole']) User type New Field */
        if ( isset($data['repairData']['repairDataOriginCreationDate']) ) $job_fields['DateBooked'] = date( 'Y-m-d', strtotime (str_replace('/','.',$data['productData']['repairDataOriginCreationDate'])));
        /* $data['repairData']['repairDataCallCenterAgentID'] map to UserID */
        /* $data['repairData']['repairDataTypeOfOrder'] map to Service Type */
        /* $data['repairData']['repairDataHandlingType'] map to Guarantee Types */
        /* $data['repairData']['repairDataHandlingCustomerGroup'] map to Repair Location*/
        if ( isset($data['repairData']['repairDataErrorDescription']) ) $job_fields['ReportedFault'] = $data['repairData']['repairDataErrorDescription'];
        if ( isset($data['repairData']['repairDataConditionCode']) ) $job_fields['ConditionCode'] = $data['repairData']['repairDataConditionCode'];
        if ( isset($data['repairData']['repairDataErrorSymptomCode']) ) $job_fields['SymptomCode'] = $data['repairData']['repairDataErrorSymptomCode'];
        /* $data['repairData']['repairDataErrorSymptomCode'] First part repair code  */
        if ( isset($data['repairData']['repairDataSoftwareVersion_new']) ) $job_fields['NewFirmwareVersion'] = $data['repairData']['repairDataSoftwareVersion_new'];
        if ( isset($data['repairData']['repairConsumerDataConsent']) && $data['repairData']['repairConsumerDataConsent'] = 'Y' ) {
            $customer_fields['DataProtection'] = 'Yes';
        } else {
            $customer_fields['DataProtection'] = 'No';
        }
        
        /* $data['statusData']['statusDataDescription'] to be mapped  */
        /* $data['statusData']['statusDataState'] to be mapped */
        
        /* $data['partData']['partDataMessageSequenceNumber'] count of parts ?????????? */
        if ( isset($data['partData']['partDataMessageSequenceNumber']) ) $job_fields['ReportedFault'] = $data['repairData']['repairDataErrorDescription'];
    }
}

?>
