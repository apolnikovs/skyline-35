<?php

require_once('CustomSmartyController.class.php');

/**
 * Short Description of BaseSmartyController.
 * 
 * Long description of BaseSmartyController.
 *
 * @author     Brian Etherington <b.etherington@pccsuk.com>
 * @copyright  2012 PC Control Systems
 * @link       http://www.pccontrolsystems.com
 * @version    1.0
 * 
 *  
 * Changes
 * Date        Version Author                Reason
 * 11/12/2012  1.0     Brian Etherington     Initial Version
 ******************************************************************************/

abstract class BaseSmartyController extends CustomSmartyController {
    
    public  $config;  
    public  $session;
    public  $messages;
    public  $smarty;
    public  $usrModel;
    public  $user;
    protected $lang = 'en';
    protected $debug = false;

    public function __construct() { 
        
        parent::__construct(); 
        
       /* ==========================================
        * Read Application Config file.
        * ==========================================
        */
        $this->config = $this->readConfig('application.ini');
        
       /* ==========================================
        *  Initialise Session Model
        * ==========================================
        */
        $this->session = $this->loadModel('Session'); 

        /* ==========================================
         *  Initialise Messages Model
         * ==========================================
         */
        $this->messages = $this->loadModel('Messages'); 
        
        /* =========== smarty assign =============== */
        $this->smarty->assign('ref', '');
        $this->smarty->assign('Search', '');
        $this->smarty->assign('ErrorMsg', '');
             
        /* ==========================================
         *  Initialise User Class
         * ==========================================
         */
        
        $this->usrModel = $this->loadModel('Users');
       
        if (isset($this->session->UserID)) {
            
            $this->user = $this->usrModel->GetUser( $this->session->UserID );
	    
            $this->smarty->assign('name',$this->user->Name);
            $this->smarty->assign('session_user_id', $this->session->UserID);  
            
            if($this->user->BranchName)
            {
                $this->smarty->assign('logged_in_branch_name', " - ".$this->user->BranchName." ".$this->config['General']['Branch']." ");
            }
            else
            {
                $this->smarty->assign('logged_in_branch_name', "");
            }
            
            if($this->session->LastLoggedIn)
            {
                $this->smarty->assign('last_logged_in', " - ".$this->config['General']['LastLoggedin']." ".date("l jS F Y G:i", $this->session->LastLoggedIn));
            }
            else
            {
                $this->smarty->assign('last_logged_in', '');
            } 
            
            $topLogoBrandID = $this->user->DefaultBrandID;
            $this->smarty->assign('_theme', $this->user->Skin);
            
        } else {
            
            $this->user = $this->usrModel->GetUser( '' ); // initialise user variable as not logged in user
            
            $topLogoBrandID = isset($_COOKIE['brand'])?$_COOKIE['brand']:0;
            
            $this->smarty->assign('session_user_id','');
            $this->smarty->assign('name','');
            $this->smarty->assign('last_logged_in','');  
            $this->smarty->assign('logged_in_branch_name', '');            
            $this->smarty->assign('_theme', 'skyline');          
        }
        
        $this->smarty->assign('loggedin_user', $this->user);
        
        if($topLogoBrandID)
        {
            $skyline = $this->loadModel('Skyline');
            $topBrandLogo = $skyline->getBrand($topLogoBrandID);

            $this->smarty->assign('_brandLogo', (isset($topBrandLogo[0]['BrandLogo']))?$topBrandLogo[0]['BrandLogo']:'');
        }
        else
        {
            $this->smarty->assign('_brandLogo', '');
        }
        $this->smarty->assign('showTopLogoBlankBox', false);
        
	//Access permissions and smarty variables are initiated/assigned here.	
	$this->usrModel->initiatePermissions($this);
        
        // check language setting
        if ($this->session->lang != '') $this->lang = $this->session->lang;
	
    }
}

?>
